						
							jQuery(function($) {


							//постановка переключателей-слайдеров в режиме расчета заказа
									 $(document).on('initSlider', ' div.curr_order.countorder .order_table  .right_col .count_params .param_mode',function () {
    $(this).slider({
								 min: 0,
								max: 100,
								step: 100,
								value:0,
								animate: '3000',
								change: function(event, ui) {
								  valuestep=$(this ).slider( 'option', 'value' );
								  
								  el=$(this);
								  
								  
									  if(valuestep==100)
									  {
										
										$(el).switchClass( 'param_mode', 'param_mode2', 100 ); 
										$(el).children('span').html('Да');
										$(el).children('a').addClass('right');	
										check_id=parseInt($('.countorder .num_order').html().trim().replace('№',''));
										if (el.parent().attr('class')=='count_music')
										{ 
												music=$('.countorder .counters_summ .music span').html();
											
											if(music=='')
											{
												$.ajax({
												type: 'POST',
												url: 'index.php?r=/site/reloadwidget',
												data: 'widgetName=CheckMiniWidget&count_music=1&check_id='+check_id,
												success: function(html){										
													$('.countorder .counters_summ .music span').html(html);
													summ=parseFloat($('.countorder .summ_order_all .summ span').html());
													$('.countorder .summ_order_all .summ span').html((summ+parseFloat(html)).toFixed(2));
														},
													});
											}
											else
											{
											 itogo=parseFloat($('.countorder .counters_summ .summ_order .summ span').html());
											 service=parseFloat($('.countorder .counters_summ  .service_order span').html());
											 discount=parseFloat($('.countorder .counters_summ  .discount span').html());
											 if (!service) {service=0;}
											 if (!discount) {discount=0;}
											 $('.countorder .summ_order_all .summ span').html((itogo+service+parseFloat(music)-discount).toFixed(2));
											}
										
										
										$('.countorder .counters_summ .music').css('visibility','visible');
										
										
										}
										 if (el.parent().attr('class')=='count_service')
										{ 	people=parseFloat($('.countorder .bottom_block .count_peoples span').html());
											summservice=parseFloat($('div.curr_order.countorder .counters_name_block .service_order .summservice').html());
											modeservice=$('div.curr_order.countorder .counters_name_block .service_order .modeservice').html();
											if (people || modeservice!='c./чел.')
											{
											if ($('.countorder .counters_summ .music').css('visibility')=='visible')
											{ music=parseFloat($('.countorder .counters_summ .music span').html()); }
											else music=0;
											itogo=parseFloat($('.countorder .counters_summ .summ_order .summ span').html());
											discount=parseFloat($('.countorder .counters_summ  .discount span').html());
											if (!discount) {discount=0;}
											if (!music) {music=0;}
											if (modeservice=='c./чел.')
											{service=people*summservice;}
											else { service=(itogo+music)*0.01*summservice;}
											$('.countorder .counters_summ  .service_order span').html(service.toFixed(2));
											$('.countorder .counters_summ .service_order').css('visibility','visible');
											$('.countorder .summ_order_all .summ span').html((itogo+music+service-discount).toFixed(2));
											}
										
										}
											
									  }
									  if(valuestep==0)
									  {
									  $(el).switchClass( 'param_mode2', 'param_mode', 100 );
									 $(el).children('span').html('Нет'); 
									 $(el).children('a').removeClass('right');
									 if (el.parent().attr('class')=='count_music')
										{ 
												$('.countorder .counters_summ .music').css('visibility','hidden');												
												music=parseFloat($('.countorder .counters_summ .music span').html());
											itogo=parseFloat($('.countorder .counters_summ .summ_order .summ span').html());
											 service=parseFloat($('.countorder .counters_summ  .service_order span').html());
											 discount=parseFloat($('.countorder .counters_summ  .discount span').html());
											 if (!service) {service=0;}
											 if (!discount) {discount=0;}
											$('.countorder .summ_order_all .summ span').html((itogo+service-discount).toFixed(2));
										
										}
									 if (el.parent().attr('class')=='count_service')
										{ 
											if ($('.countorder .counters_summ .music').css('visibility')=='visible')
											{ music=parseFloat($('.countorder .counters_summ .music span').html()); }
											else music=0;
											itogo=parseFloat($('.countorder .counters_summ .summ_order .summ span').html());
											discount=parseFloat($('.countorder .counters_summ  .discount span').html());
											if (!discount) {discount=0;}
											if (!music) {music=0;}
											
											$('.countorder .counters_summ  .service_order span').html(0);
											$('.countorder .summ_order_all .summ span').html((itogo+music-discount).toFixed(2));
										
										}
									  }
								  },
							 
							});
						  });
						  
						  //выбор количества людей за столиком из готового списка в режиме расчета заказа
						  $(document).on('click', 'div.curr_order.countorder .count_people .peoples .listpeoples div[class!="line"],div.curr_order.countorder .count_people .peoples  div.first',function() 
						  {
							people=parseFloat($(this).html().replace('<span>чел.</span>',''));
							$('div.curr_order.countorder .count_people .peoples div[class!="line"]').removeClass('active');
							$(this).addClass('active');
							var flag_count=$('#count_service').slider("option","value");
							if (!people) {people=0; $('div.curr_order.countorder .bottom_block .count_peoples span').html('') }
							else {
								$('div.curr_order.countorder .bottom_block .count_peoples span').html(people); }
				
							if (flag_count==100)
								{															
								summservice=parseFloat($('div.curr_order.countorder .counters_name_block .service_order .summservice').html());
								modeservice=$('div.curr_order.countorder .counters_name_block .service_order .modeservice').html();
								
								if ($('.countorder .counters_summ .music').css('visibility')=='visible')
											{ music=parseFloat($('.countorder .counters_summ .music span').html()); }
											else music=0;
											itogo=parseFloat($('.countorder .counters_summ .summ_order .summ span').html());
											discount=parseFloat($('.countorder .counters_summ  .discount span').html());
											if (!discount) {discount=0;}
											if (!music) {music=0;}
											
								 if (modeservice=='c./чел.')
								 { 			service=people*summservice;
											
								 }
								
											$('.countorder .counters_summ  .service_order span').html(service.toFixed(2));
											$('.countorder .counters_summ .service_order').css('visibility','visible');
											$('.countorder .summ_order_all .summ span').html((itogo+service+music-discount).toFixed(2));
								
							
							}
						  });
						  
						  //пролистывание количества людей вперед в режиме расчета заказа
						   $(document).on('click','div.curr_order.countorder .count_people .peoples .arrow', function()
						   {
						   el=$('div.curr_order.countorder .count_people .peoples .listpeoples');
							smeschenie=parseInt(el.css('margin-left').trim().replace('px',''));
							
							if (Math.abs(smeschenie)<295)
							el.css('margin-left',-(Math.abs(smeschenie)+295));
							if (Math.abs(smeschenie)+295==295)
							{ $('div.curr_order.countorder .count_people .peoples .first').removeClass('first').addClass('backarrow');
							$('div.curr_order.countorder .count_people .peoples .backarrow').html('');
							$(this).removeClass('arrow').addClass('keyboard');
							}
						   });
						   
						   //пролистывание количества людей назад в режиме расчета заказ
						   $(document).on('click', 'div.curr_order.countorder .count_people .peoples .backarrow',function()
						   {
						   el=$('div.curr_order.countorder .count_people .peoples .listpeoples');
							smeschenie=parseInt(el.css('margin-left').trim().replace('px',''));							
							 
							if (smeschenie<0)
							el.css('margin-left',smeschenie+295); 
							if (smeschenie+295==0)
							{
							$(this).removeClass('backarrow').addClass('first');
							$(this).html('нет');
							$('div.curr_order.countorder .count_people .peoples .keyboard').removeClass('keyboard').addClass('arrow');
							}
						   });
						   
						   //вызов окна ввода произвольного количества человек за столиком в режиме расчета заказа
						   $(document).on('click', 'div.curr_order.countorder .count_people .peoples .keyboard',function()
						   {	klava=$('.keyboard_window');
							 if (!klava.html()) 
							 {
								info='<p>Введите количество человек</p>';
								$.ajax({
											type: 'POST',
											url: 'index.php?r=/site/reloadwidget',
											data: 'widgetName=KeyboardWidget&info='+info,
											success: function(html){
											$('<div class="keyboard_window">'+html+'</div>').insertAfter('div.curr_order.countorder .count_people .peoples .keyboard');
											
											},
										});
								}
							else
							{
								klava.remove();
							}
						   });
						   
						   //ввод произвольного количества человека за столиком в режиме расчета заказа
						   $(document).on('submit', 'div.curr_order.countorder .count_people .peoples .keyboard_window form',function()
						   {	klava=$('.keyboard_window');
								$('div.curr_order.countorder .count_people .peoples div[class!="line"]').removeClass('active');
								people=parseInt(klava.find('input.number').val());
								$('div.curr_order.countorder .bottom_block .count_peoples span').html(people);
								var flag_count=$('#count_service').slider("option","value");
								if (flag_count==100)
								{															
								summservice=parseFloat($('div.curr_order.countorder .counters_name_block .service_order .summservice').html());
								modeservice=$('div.curr_order.countorder .counters_name_block .service_order .modeservice').html();
								
								if ($('.countorder .counters_summ .music').css('visibility')=='visible')
											{ music=parseFloat($('.countorder .counters_summ .music span').html()); }
											else music=0;
											itogo=parseFloat($('.countorder .counters_summ .summ_order .summ span').html());
											discount=parseFloat($('.countorder .counters_summ  .discount span').html());
											if (!discount) {discount=0;}
											if (!music) {music=0;}
											
								 if (modeservice=='c./чел.')
								 { 			service=people*summservice;
											
								 }
								
											$('.countorder .counters_summ  .service_order span').html(service.toFixed(2));
											$('.countorder .counters_summ .service_order').css('visibility','visible');
											$('.countorder .summ_order_all .summ span').html((itogo+service+music-discount).toFixed(2));
								
							
							}
								klava.remove();
							 return false;
						   });
						   
						   //подсчет скидки в режиме расчета заказа
						  $(document).on('click', 'div.curr_order.countorder .count_discount .disscounts div',function() 
						  {	$('div.curr_order.countorder .count_discount .disscounts div').removeClass('active');
							$(this).addClass('active');
							if ($('.countorder .counters_summ .music').css('visibility')=='visible')
											{ music=parseFloat($('.countorder .counters_summ .music span').html()); }
											else music=0;
											itogo=parseFloat($('.countorder .counters_summ .summ_order .summ span').html());
											service=parseFloat($('.countorder .counters_summ .service_order span').html());
											discount=parseFloat($(this).html().replace('%',''));
											if (!discount) {discount=0;}
											discount=itogo*0.01*discount;
											if (!music) {music=0;}								
											if (!service) {service=0;}								
											$('.countorder .counters_summ  .discount span').html(discount.toFixed(2));
											$('.countorder .counters_summ .discount').css('visibility','visible');
											$('.countorder .summ_order_all .summ span').html((itogo+service+music-discount).toFixed(2));
								
							
							
						  });
						  $(document).on('click', 'div.curr_order.countorder .esc_button',function()
						  {
								
							$.ajax({
											type: 'POST',
											url: 'index.php?r=/site/reloadwidget',
											data: 'widgetName=CheckMiniWidget',
											success: function(html){
											$('div.curr_order').removeClass('countorder');
											$('.curr_order').html(html);
											},
										});
						  });
						  
						  //таймер на проверку входящих заказов на передачу
							TimerTransferTo=setInterval(function () { transferCheckTo(); }, 20000);	
							
							//отправка на кухню заказа
								$(document).on('click',' .buttons_order .ok',function(e)
								{ 
								curr_order=$(this).parent().parent().parent();
								classparent=curr_order.parent().attr('class');
									
									if (classparent=='one_order' || classparent=='one_order current')
									{	
										check_id=curr_order.find('.num_order').html().trim().replace('№','');
										table_id=curr_order.find('.num_table').html().trim();
										params='&check_id='+check_id+'&table_id='+table_id+'&orders=all';
										if ($('.curr_order .num_order').length)
										curr_id=$('.curr_order .num_order').html().trim().replace('№','');
										else
										curr_id='';
									}
									else
									{params=''; check_id=''; curr_id='false';}
									$('div.progressbar').show();
									$.ajax({
											type: 'POST',
											url: 'index.php?r=/site/reloadwidget',
											data: 'widgetName=CheckMiniWidget&status=2'+params,
											success: function(html){
												if (classparent=='one_order' || classparent=='one_order current')
												{$('.list_orders').html(html);}
												else
												{$('.curr_order').html(html);}
											},
										});
										if (check_id==curr_id)
										{ 
											$.ajax({
											type: 'POST',
											url: 'index.php?r=/site/reloadwidget',
											data: 'widgetName=CheckMiniWidget',
											success: function(html){
												$('.curr_order').html(html);
											},
										});
										}
									if ((classparent!='one_order' && classparent!='one_order current') || check_id==curr_id)
									{
										modelist=$('.items div.mode').attr('class');
										cat_id=$('.items span.catsel').html();
										$.ajax({
												type: 'POST',
												url: 'index.php?r=/site/reloadwidget',
												data: 'widgetName=ItemsListWidget&mode_list='+modelist+'&cat_id='+cat_id,
												success: function(html){
												$('.items').html(html); 
												myScroll = new iScroll('scrollitems');},
												error:  function(xhr, str){												
												alert('Возникла ошибка: ' + xhr.responseCode);},
												
												
											});
									}
									$('div.progressbar').hide();
									e.stopPropagation();
								});
								
								
								//удаление не отправленного на кухню заказа
								 $(document).on('click',' .buttons_order .esc',function(e)
								{	
									$('div.progressbar').show();
									
									curr_order=$(this).parent().parent().parent();
								classparent=curr_order.parent().attr('class');
									
									if (classparent=='one_order' || classparent=='one_order current')
									{
										check_id=curr_order.find('.num_order').html().trim().replace('№','');
										table_id=curr_order.find('.num_table').html().trim();
										params='&check_id='+check_id+'&table_id='+table_id+'&orders=all';
										if ($('.curr_order .num_order').length)
										curr_id=$('.curr_order .num_order').html().trim().replace('№','');
										else
										curr_id='';
									}
									else
									{params=''; check_id=''; curr_id='false';}
									$.ajax({
											type: 'POST',
											url: 'index.php?r=/site/reloadwidget',
											data: 'widgetName=CheckMiniWidget&status=del'+params,
											success: function(html){										
												if (classparent=='one_order' || classparent=='one_order current')
												{$('.list_orders').html(html);}
												else
												{$('.curr_order').html(html);}
											},
											
										});
									if (check_id==curr_id)
										{ 
											$.ajax({
											type: 'POST',
											url: 'index.php?r=/site/reloadwidget',
											data: 'widgetName=CheckMiniWidget',
											success: function(html){
												$('.curr_order').html(html);
											},
										});
										}
									if (classparent!='one_order' && classparent!='one_order current')
									{
										modelist=$('.items div.mode').attr('class');
										cat_id=$('.items span.catsel').html();
										$.ajax({
												type: 'POST',
												url: 'index.php?r=/site/reloadwidget',
												data: 'widgetName=ItemsListWidget&mode_list='+modelist+'&cat_id='+cat_id,
												success: function(html){
												$('.items').html(html);
												myScroll = new iScroll('scrollitems');
												},
												error:  function(xhr, str){
												alert('Возникла ошибка: ' + xhr.responseCode);},
												
											});
									}
									$('div.progressbar').hide();
									e.stopPropagation();
								});
								
								//распечатка чека
								 $(document).on('click',' .countorder .print',function(e)
								{ 
									
																		
									param=$(this).attr('id');
									
									countmusic=0;
									countdiscount=0;
									countpeople=0;
									countservice=0;
									itogo=0;
									copy=0;
									parameters='';
									blockmusic=$('.countorder .counters_summ .music');
									blockdiscount=$('.countorder .counters_summ .discount');
									blockpeople=$('.countorder .bottom_block .count_peoples');
									blockservice=$('.countorder .counters_summ .service_order');
									;
									if (blockmusic.css('visibility')=='visible')
									{countmusic=parseFloat(blockmusic.children('span').html());
									
									}
									if (blockdiscount.css('visibility')=='visible')
									countdiscount=parseFloat(blockdiscount.children('span').html());

									countpeople=parseInt(blockpeople.children('span').html())
									
									if (blockservice.css('visibility')=='visible')
									countservice=parseFloat(blockservice.children('span').html());
									itogo=parseFloat($('.countorder .summ_order_all .summ span').html());
									
									if (!countmusic) countmusic=0;
									if (!countdiscount) countdiscount=0;
									if (!countpeople) countpeople=0;
									if (!countservice) countservice=0;
									if (!itogo) itogo=0;
									
									var flag_count=$('#copy').slider("option","value");
									if (flag_count==100) copy=1;
									
									parameters='&music='+countmusic+'&discount='+countdiscount+'&people='+countpeople+'&service='+countservice+'&itogo='+itogo+'&copy='+copy;
									
									$('div.progressbar').show();
									$('.curr_order').removeClass('countorder');
									$('.curr_order').html('');
									$.ajax({
											type: 'POST',
											url: 'index.php?r=/site/reloadwidget',
											data: 'widgetName=CheckMiniWidget&status=pay'+parameters,
											success: function(html){
													if (param=='go')
													{
													
													$('.top_buttons .orders .noactive').removeClass().addClass('active');
													$('.top_buttons .orders .active .list_orders').show("blind",{},600);
													$.ajax({
														type: 'POST',
														url: 'index.php?r=/site/reloadwidget',
														data: 'widgetName=CheckMiniWidget&orders=all',
														success: function(html){
														$('div.progressbar').hide();					
														$('.top_buttons .orders .list_orders').html(html);
														},
														
														});
													}	
											},	
										});
										if (param=='exit') window.location = "/site/logout";
										modelist=$('.items div.mode').attr('class');
										cat_id=$('.items span.catsel').html();
										$.ajax({
												type: 'POST',
												url: 'index.php?r=/site/reloadwidget',
												data: 'widgetName=ItemsListWidget&mode_list='+modelist+'&cat_id='+cat_id,
												success: function(html){
												$('.items').html(html);
												myScroll = new iScroll('scrollitems');
												},
												error:  function(xhr, str){												
												alert('Возникла ошибка: ' + xhr.responseCode);},
											});
										
									
									$('div.progressbar').hide();
									
								});
								
								//расчет заказа
								 $(document).on('click',' .buttons_order .calc',function(e)
								{ 
									$('div.progressbar').show();
									curr_order=$(this).parent().parent().parent();
								classparent=curr_order.parent().attr('class');
									
									if (classparent=='one_order' || classparent=='one_order current')
									{
										check_id=curr_order.find('.num_order').html().trim().replace('№','');
										table_id=curr_order.find('.num_table').html().trim();
										params='&check_id='+check_id+'&table_id='+table_id+'&orders=changecurrent';
										if ($('.curr_order .num_order').length)
										curr_id=$('.curr_order .num_order').html().trim().replace('№','');
										else
										curr_id='';
									}
									else
									{params='';}
								
									$.ajax({
											type: 'POST',
											url: 'index.php?r=/site/reloadwidget',
											data: 'widgetName=CheckMiniWidget&countorder=1'+params,
											success: function(html){
												if (classparent=='one_order' || classparent=='one_order current')
												{jQuery('.top_buttons .orders .list_orders').html('');			
				$('.top_buttons .orders .active .list_orders').hide("blind",{},100, function()
				{$('.top_buttons .orders .active').removeClass().addClass('noactive'); });}
												$('.curr_order').addClass('countorder');
												$('.curr_order').html(html);
												$(' div.curr_order.countorder .order_table  .right_col .count_params > div > div').trigger('initSlider');
												$('.countorder #count_service').slider( 'option', 'value',100 );
											},
											
										});
									
									
									$('div.progressbar').hide();
									e.stopPropagation();
								});
								
								//выбор текущего заказа
								$(document).on('click','.one_order',function()
									{
										if ($(this).attr('class')=='one_order current')
										{
											alert('Этот заказ уже является текущим.');
										}
										else
										{	$('div.progressbar').show();
											check_id=$(this).find('.num_order').html().trim().replace('№','');
											table_id=$(this).find('.num_table').html().trim();
											params='&check_id='+check_id+'&table_id='+table_id+'&orders=changecurrent';
											modelist=$('.items div.mode').attr('class');
											cat_id=$('.items span.catsel').html();
											$.ajax({
											type: 'POST',
											url: 'index.php?r=/site/reloadwidget',
											data: 'widgetName=CheckMiniWidget'+params,
											success: function(html){
											$('.curr_order').html(html);
											},
											error:  function(xhr, str){												
												alert('Возникла ошибка: ' + xhr.responseCode);},
											});
											$.ajax({
												type: 'POST',
												url: 'index.php?r=/site/reloadwidget',
												data: 'widgetName=ItemsListWidget&mode_list='+modelist+'&cat_id='+cat_id,
												success: function(html){
												$('.items').html(html);
												myScroll = new iScroll('scrollitems');
												},
												error:  function(xhr, str){												
												alert('Возникла ошибка: ' + xhr.responseCode);},
											});
										$('div.progressbar').hide();
										jQuery('.top_buttons .orders .list_orders').html('');			
										$('.top_buttons .orders .active .list_orders').hide("blind",{},600, function()
										{$('.top_buttons .orders .active').removeClass().addClass('noactive'); });
										}
									});
									
									//создание нового заказа
									$(document).on('click','.one_orderadd',function()
									{
											$('div.progressbar').show();
											
											params='&orders=changecurrent';
											modelist=$('.items div.mode').attr('class');
											cat_id=$('.items span.catsel').html();
											$.ajax({
											type: 'POST',
											url: 'index.php?r=/site/reloadwidget',
											data: 'widgetName=CheckMiniWidget'+params,
											success: function(html){
											$('.curr_order').html(html);
											},
											error:  function(xhr, str){												
												alert('Возникла ошибка: ' + xhr.responseCode);},
											});
											$.ajax({
												type: 'POST',
												url: 'index.php?r=/site/reloadwidget',
												data: 'widgetName=ItemsListWidget&mode_list='+modelist+'&cat_id='+cat_id,
												success: function(html){
												$('.items').html(html);
												myScroll = new iScroll('scrollitems');
												},
												error:  function(xhr, str){												
												alert('Возникла ошибка: ' + xhr.responseCode);},
											});
										$('div.progressbar').hide();
										jQuery('.top_buttons .orders .list_orders').html('');			
										$('.top_buttons .orders .active .list_orders').hide("blind",{},600, function()
										{$('.top_buttons .orders .active').removeClass().addClass('noactive'); });
										
									});
								
								// переход в режим изменения заказа
								$(document).on('click','.curr_order .change_button, .curr_order .change_order',function()
								{			 
									if ( $('.curr_order').attr('class')=='curr_order divide')
									{
										$.ajax({
												type: 'POST',
												url: 'index.php?r=/site/reloadwidget',
												data: 'widgetName=CheckMiniWidget',
												success: function(html){
												$('.curr_order').html(html);
												},
												error:  function(xhr, str){												
												alert('Возникла ошибка: ' + xhr.responseCode);},
											});
									}
									 $('.curr_order div').removeClass('current');
									$('.curr_order .mode_order div:first-child').addClass('current'); 
									$('.curr_order').removeClass().addClass('curr_order').addClass('change');
									$('.curr_order').show('drop',{direction:"right"},1000);

									
									$('.curr_order .list_order .str.send').each( function()
									{
										id=$(this).children('div.name').attr('id');
										was_count=parseInt($(this).children('div.count').html());
										that=$('.curr_order .list_order .str[class!=\'str send\'] #'+id);
										count=0;
										that.each( function()
										{
										count=count+parseInt(that.next('.count').html());
										
										});
										if (count!=0 && (was_count+count)==0)
										{
										$(this).children('div.esc').html('');
										$(this).children('div.esc').removeClass().addClass('other');
										}
									});
								});
								
								//переход в режим разделения заказа
								$(document).on('click','.curr_order .divide_order',function()
								{	$('.curr_order div').removeClass('current');
									$(this).addClass('current');
									
									
									if ($('.curr_order').attr('class')=='curr_order change')
									{
										$.ajax({
												type: 'POST',
												url: 'index.php?r=/site/reloadwidget',
												data: 'widgetName=CheckMiniWidget',
												success: function(html){
												$('.curr_order').html(html);
												$('.curr_order div').removeClass('current');
												$('.curr_order .divide_order').addClass('current');
												$('.curr_order').removeClass().addClass('curr_order').addClass('divide');
												if ($('.curr_order  .list_order_new .str').length==0)
									{ 
									$('.curr_order  .list_order .str').each(
									
									function() { 
								    if ($(this).html()=='&nbsp;')
									{
									$('.curr_order .order_table .list_order_new').
									append('<div class=\"str\">&nbsp;</div>');
									}
									else
									{
									
									if($(this).attr('class')=='str send')
									{
									$('.curr_order .order_table .list_order_new').
									append('<div class=\"str send\"><div class=\"str_left\"></div><div class=\"name\"></div><div class=\"count\"></div><div class=\"summ\"></div></div>');
									}
									else
									{
									$('.curr_order .order_table .list_order_new').
									append('<div class=\"str\"><div class=\"str_left\"></div><div class=\"name\"></div><div class=\"count\"></div><div class=\"summ\"></div></div>');
									}
									}
									});
									}
												},
												error:  function(xhr, str){												
												alert('Возникла ошибка: ' + xhr.responseCode);},
											});
									}
									else
									{
									$('.curr_order').removeClass().addClass('curr_order').addClass('divide');
									if ($('.curr_order  .list_order_new .str').length==0)
									{ 
									$('.curr_order  .list_order .str').each(
									
									function() { 
								    if ($(this).html()=='&nbsp;')
									{
									$('.curr_order .order_table .list_order_new').
									append('<div class=\"str\">&nbsp;</div>');
									}
									else
									{
									if($(this).attr('class')=='str send')
									{
									$('.curr_order .order_table .list_order_new').
									append('<div class=\"str send\"><div class=\"str_left\"></div><div class=\"name\"></div><div class=\"count\"></div><div class=\"summ\"></div></div>');
									}
									else
									{
									$('.curr_order .order_table .list_order_new').
									append('<div class=\"str\"><div class=\"str_left\"></div><div class=\"name\"></div><div class=\"count\"></div><div class=\"summ\"></div></div>');
									}
									}
									});
									}
									}
									
								});
$(document).on("dblclick", ".curr_order.divide .order_table  .str",function(el) {

	
$(el.target).data('dblclicked','no');	

$(this).mousemove(function(e) {
if ($(el.target).data('dblclicked')!='yes')
{
X = e.pageX-$(this).offset().left;
Y = e.pageY-$(this).offset().top;


str=$(this);
parent=str.parent().attr('class');
if(str.children('.name').html()!='')
{
index=str.index();
if (parent=='list_order')
{
str_new=$('.curr_order.divide .list_order_new .str').eq(index); }
else
str_new=$('.curr_order.divide .list_order .str').eq(index);
name=str.children('.name').html();
id=str.children('.name').attr('id');
count=parseInt(str.children('.count').html());
summ=parseInt(str.children('.summ').html());
name_new=str_new.children('.name');
count_new=str_new.children('.count');
summ_new=str_new.children('.summ');
if (count>0)
{
	if (name_new.html()=='')
	{
		name_new.html(name).attr('id',id);
		count_new.html(count);
		summ_new.html(summ);
		str.children('.name').html('');
		str.children('.count').html('');
		str.children('.summ').html('');
	}
	else
	{	
		name_new.html(name).attr('id',id);
		count_new.html(count+parseInt(count_new.html()));
		summ_new.html(summ+parseInt(summ_new.html()));
		str.children('.name').html('');
		str.children('.count').html('');
		str.children('.summ').html('');
	}
}
else
{alert('Нельзя переместить отмену блюда');}
}
$(el.target).data('dblclicked','yes');	
}
	});
	
});

$(document).on('click','.curr_order.divide .info_order_new .select_table',function()
{ 

if (!$('.info_order_new div.listOfTables').html())
{ $(this).after('<div class="listOfTables"></div>'); 
		$.ajax({
				type: 'POST',
				url: 'index.php?r=/site/reloadwidget',
				data: 'widgetName=TablesListWidget&visible=1&selectOtherTable=1',
				success: function(html){
				$('.info_order_new div.listOfTables').html(html);
					}
					
				});
}
else
{
$('.info_order_new div.listOfTables').remove();
}
});
$(document).on('click', 'div.curr_order.divide  .listOfTables  .listtabs .tab, div.curr_order.move  .listOfTables  .listtabs .tab',function() 
{
				  el=$(this).attr('id').replace('halltab','');
				  $(this).parent().children('.tab').removeClass('active');
				  $(this).addClass('active');
				   $(this).parent().parent().children('.inner_list').removeClass('active');
				   $(this).parent().parent().children('#hall'+el).addClass('active');
						});
$(document).on('click','div.curr_order.divide  .listOfTables .all_tables .view',function() {
table_id=$(this).attr('id').replace('table','');
status=$(this).find('.statustable').html();
countcheck=$(this).find('.countcheckstable').html();
check_id=$(this).find('.check_idtable').html();
$('div.curr_order.divide .info_order_new div.select_table').remove();
$('div.curr_order.divide .info_order_new div.num_table').html(table_id);
$('div.curr_order.divide .info_order_new div.num_table').after('<span class="statustable">'+status+'</div>');
$('div.curr_order.divide .info_order_new div.num_table').after('<span class="countcheckstable">'+countcheck+'</div>');
$('div.curr_order.divide .info_order_new div.num_table').after('<span class="check_idtable">'+check_id+'</div>');
$('div.listOfTables').remove();
});
								
								
								$('.curr_order.divide .list_order .str').live('click',function()
								{ 
								var otmena=0;
								str=$(this); 
								if(str.children('.name').html()!='')
									{
										
										index=str.index();
										str_new=$('.curr_order.divide .list_order_new .str').eq(index);
										name=str.children('.name').html();
										id=str.children('.name').attr('id');
										count=parseInt(str.children('.count').html());
										summ=parseInt(str.children('.summ').html());
										
										name_new=str_new.children('.name');
										count_new=str_new.children('.count');
										summ_new=str_new.children('.summ');
								if (count>0)
								{
									if (str.attr('class')=='str send')
									{
										$('.curr_order.divide .list_order div.str[class!="str send"] div#'+id).each(
										function(){
										
										countesc=parseInt($(this).parent().children('.count').html());
										if (countesc+count<=0) otmena=1;
										
										});
									
									}
									
									if (otmena==0)
									{
										if(name_new.html()=='')
										{
										
										name_new.html(name).attr('id',id);
										count_new.html(count/Math.abs(count));
										summ_new.html(summ/Math.abs(count));
										}
										else
										{
										count_new.html(parseInt(count_new.html())+count/Math.abs(count));
										summ_new.html(parseInt(summ_new.html())+(summ/Math.abs(count)));
										}
										change_count=(Math.abs(count)-1)*count/Math.abs(count);
										summ_count=(Math.abs(summ)-summ/count)*count/Math.abs(count);
										if(change_count==0)
										{
											str.children('.name').html('');
											str.children('.count').html('');
											str.children('.summ').html('');
										}
										else
										{
										str.children('.count').html(change_count);
										str.children('.summ').html(summ_count);
										}
									}
									else
									{alert('Блюдо отменено. Перемещение невозможно.');}
								}
								else
								{alert('Нельзя переместить отмену блюда');}
									}
								});
								
								$('.curr_order.divide .list_order_new .str').live('click',function()
								{ 
								str=$(this); 
								if(str.children('.name').html()!='')
									{
										
										index=str.index();
										str_new=$('.curr_order.divide .list_order .str').eq(index);
										name=str.children('.name').html();
										id=str.children('.name').attr('id');
										count=parseInt(str.children('.count').html());
										summ=parseInt(str.children('.summ').html());
										
										name_new=str_new.children('.name');
										count_new=str_new.children('.count');
										summ_new=str_new.children('.summ');
										if (count>0)
									{
										if(name_new.html()=='')
										{
										
										name_new.html(name).attr('id',id);
										count_new.html(count/Math.abs(count));
										summ_new.html(summ/Math.abs(count));
										}
										else
										{
										count_new.html(parseInt(count_new.html())+count/Math.abs(count));
										summ_new.html(parseInt(summ_new.html())+(summ/Math.abs(count)));
										}
										change_count=(Math.abs(count)-1)*count/Math.abs(count);
										summ_count=(Math.abs(summ)-summ/count)*count/Math.abs(count);
										if(change_count==0)
										{
											str.children('.name').html('');
											str.children('.count').html('');
											str.children('.summ').html('');
										}
										else
										{
										str.children('.count').html(change_count);
										str.children('.summ').html(summ_count);
										}
									}
									else
									{alert('Нельзя переместить отмену блюда');}
									}
								});
								
								
								$('.curr_order.divide .ready_button').live('click',function()
								{
									status=$('div.curr_order.divide .info_order_new span.statustable').html();
									if($('div.curr_order.divide .info_order_new span.statustable').html()==null)
									{alert('Вы не выбрали столик! Разделение заказа не будет осуществлено!');}
									else
									{
										i=0;
										item_id=[];
										count=[];	
										price=[];	
										alreadysend=[];
										modelist=$('.items div.mode').attr('class');
										countcheck=$('div.curr_order.divide .info_order_new span.countcheckstable').html();
										check_id=$('div.curr_order.divide .info_order_new span.check_idtable').html();
										table_id=$('div.curr_order.divide .info_order_new .num_table').html();
										cat_id=$('.items span.catsel').html();
										$('.curr_order .list_order_new .str').each(function()
										{
											name=$(this).children('.name');
											
											
											if ($(this).children('.name').html()!=null && $(this).children('.name').html()!='')
											{
												if ($(this).attr('class')=='str send') el_alreadysend=1; else el_alreadysend=0;
												el_item_id=$(this).children('.name').attr('id').replace('itemid','');
												el_summ=parseInt($(this).children('div.summ').html());
												el_count=parseInt($(this).children('div.count').html());
												el_price=el_summ/el_count;
										
												item_id[i]=el_item_id;	
												count[i]=el_count;	
												price[i]=el_price;
												alreadysend[i]=el_alreadysend;	
												i++;
											}
											
											
										});
										if (i==0)
										{alert('Вы не выбрали не одной позиции для перемещения! Рзделение заказа не будет осуществлено!');}
										else
										{
										 $('div.progressbar').show();
									
											$.ajax({
													type: 'POST',
													url: 'index.php?r=/site/reloadwidget',
													data: 'widgetName=CheckMiniWidget&item_id='+item_id+'&price='+price+'&count='+count+'&alreadysend='+alreadysend+'&divideOrder=1&statusTable='+status+'&countcheckTable='+countcheck+'&check_idTable='+check_id+'&divideTable='+table_id,
													success: function(html){
													alert('Ваш заказ успешно разделен');
													$('.curr_order').html(html);
													},
													error:  function(xhr, str){
													alert('Возникла ошибка: ' + xhr.responseCode);},
												});
											$.ajax({
													type: 'POST',
													url: 'index.php?r=/site/reloadwidget',
													data: 'widgetName=ItemsListWidget&mode_list='+modelist+'&cat_id='+cat_id,
													success: function(html){
													$('div.progressbar').hide('fast');
													$('.items').html(html);
													myScroll = new iScroll('scrollitems');
													},
													error:  function(xhr, str){
													$('div.progressbar').hide('fast');
													alert('Возникла ошибка: ' + xhr.responseCode);},
												});
									}
										}
									
									
									$('.curr_order').removeClass('divide');
									
									
								});
								
								
								/*режимы переноса заказа*/
								$('.curr_order .move_order').live('click',function()
								{	
									if ( $('.curr_order').attr('class')=='curr_order divide' || $('.curr_order').attr('class')=='curr_order change')
									{
										$.ajax({
												type: 'POST',
												url: 'index.php?r=/site/reloadwidget',
												data: 'widgetName=CheckMiniWidget',
												success: function(html){
												$('.curr_order').html(html);
												$('.curr_order div').removeClass('current');
												$('.curr_order .move_order').addClass('current');
												},
												error:  function(xhr, str){												
												alert('Возникла ошибка: ' + xhr.responseCode);},
											});
									}
									$('.curr_order div').removeClass('current');
									$(this).addClass('current');
									$('.curr_order').removeClass().addClass('curr_order').addClass('move');
									
									
								});
								$('.curr_order.move .move_order_table .select_table').live('click',function()
{ 

if (!$('.move_order_table  div.listOfTables').html())
{
{ $(this).after('<div class="listOfTables"></div>'); }
		$.ajax({
				type: 'POST',
				url: 'index.php?r=/site/reloadwidget',
				data: 'widgetName=TablesListWidget&visible=1&selectOtherTable=1',
				success: function(html){
				$(' .move_order_table  div.listOfTables').html(html);
					}
					
				});
}
else
$('.move_order_table  div.listOfTables').remove();
});

$('div.curr_order.move  .listOfTables .all_tables .view').live('click',function() {
table_id=$(this).attr('id').replace('table','');
status=$(this).find('.statustable').html();
countcheck=$(this).find('.countcheckstable').html();
check_id=$(this).find('.check_idtable').html();
$('div.curr_order.move .move_order_table div.select_table').remove();
$('div.curr_order.move .move_order_table div.num_table').html(table_id);
$('div.curr_order.move .move_order_table div.num_table').after('<span class="statustable">'+status+'</div>');
$('div.curr_order.move .move_order_table div.num_table').after('<span class="countcheckstable">'+countcheck+'</div>');
$('div.curr_order.move .move_order_table div.num_table').after('<span class="check_idtable">'+check_id+'</div>');
$('div.listOfTables').remove();
});

$('.curr_order.move .ready_button').live('click',function()
								{
									
									if($('div.curr_order.move .move_order_table span.statustable').html()==null)
									{alert('Вы не выбрали столик! Перемещение заказа не будет осуществлено!');}
									else
									{
										i=0;
										
										modelist=$('.items div.mode').attr('class');
										status=$('div.curr_order.move .move_order_table span.statustable').html();
										countcheck=$('div.curr_order.move .move_order_table span.countcheckstable').html();
										check_id=$('div.curr_order.move .move_order_table span.check_idtable').html();
										table_id=$('div.curr_order.move .move_order_table .num_table').html();
										cat_id=$('.items span.catsel').html();
										 $('div.progressbar').show();
									
											$.ajax({
													type: 'POST',
													url: 'index.php?r=/site/reloadwidget',
													data: 'widgetName=CheckMiniWidget&moveOrder=1&statusTable='+status+'&countcheckTable='+countcheck+'&check_idTable='+check_id+'&moveTable='+table_id,
													success: function(html){
													alert('Ваш заказ успешно перемещен');
													$('.curr_order').html(html);
													},
													error:  function(xhr, str){
													alert('Возникла ошибка: ' + xhr.responseCode);},
												});
											$.ajax({
													type: 'POST',
													url: 'index.php?r=/site/reloadwidget',
													data: 'widgetName=ItemsListWidget&mode_list='+modelist+'&cat_id='+cat_id,
													success: function(html){
													$('div.progressbar').hide('fast');
													$('.items').html(html);
													myScroll = new iScroll('scrollitems');
													},
													error:  function(xhr, str){
													$('div.progressbar').hide('fast');
													alert('Возникла ошибка: ' + xhr.responseCode);},
												});
									
										}
									
									
									$('.curr_order').removeClass('move');
									
									
								});

/* режим передачи заказа */
$('.curr_order .transmit_order').live('click',function()
								{	
									
									if ( $('.curr_order').attr('class')=='curr_order divide' || $('.curr_order').attr('class')=='curr_order change')
									{		
										$.ajax({
												type: 'POST',
												url: 'index.php?r=/site/reloadwidget',
												data: 'widgetName=CheckMiniWidget',
												success: function(html){
												$('.curr_order').html(html);
												$('.curr_order div').removeClass('current');
												$('.curr_order .transmit_order').addClass('current');
												},
												error:  function(xhr, str){												
												alert('Возникла ошибка: ' + xhr.responseCode);},
											});
									}
									$('.curr_order div').removeClass('current');
									$(this).addClass('current');
									$('.curr_order').removeClass().addClass('curr_order').addClass('transmit');
																	
								});
	
								$('.curr_order.transmit .transmit_order_table .select_waiter').live('click',function()
{ 

if (!$('.transmit_order_table  div.listOfWaiters').html())
{
 $(this).after('<div class="listOfWaiters"></div>'); 
		$.ajax({
				type: 'POST',
				url: 'index.php?r=/waiter/list',
				data: '',
				success: function(html){
				$(' .transmit_order_table  div.listOfWaiters').html(html);
					}
					
				});
}
else
{$('.transmit_order_table  div.listOfWaiters').remove();}
});	
	$('div.curr_order.transmit  .listOfWaiters .one_cat a').live('click',function() {
waiter_id=$(this).attr('id').replace('waiter','');
$(this).children('span').remove();
waiter=$(this).html();
$('div.curr_order.transmit .transmit_order_table div.select_waiter').remove();
$('div.curr_order.transmit .transmit_order_table div.waiter').html(waiter);
$('div.curr_order.transmit .transmit_order_table div.waiter').attr('id','waiter'+waiter_id);
$('div.listOfWaiters').remove();
});							
	transferCheckFrom=function (transfer_id)	
{
transfer_id=parseInt(transfer_id);
$.ajax({
				type: 'POST',
				url: 'index.php?r=/waiter/transmitfrom',
				data: 'transfer_id='+transfer_id,
				success: function(msq){
				status=parseInt(msq);
				
				if (status==1)
				alert("Передача заказа не подтверждена другим официантом.");
				if (status==2)
				{ alert("Заказ успешно передан другому официанту.");
				modelist=$('.items div.mode').attr('class');
				cat_id=$('.items span.catsel').html();
				$.ajax({
											type: 'POST',
											url: 'index.php?r=/site/reloadwidget',
											data: 'widgetName=CheckMiniWidget',
											success: function(html){											
											$('.curr_order').html(html);
											},
											error:  function(xhr, str){
											alert('Возникла ошибка: ' + xhr.responseCode);},
										});
									$.ajax({
											type: 'POST',
											url: 'index.php?r=/site/reloadwidget',
											data: 'widgetName=ItemsListWidget&mode_list='+modelist+'&cat_id='+cat_id,
											success: function(html){
											$('div.progressbar').hide('fast');
											$('.items').html(html);
											myScroll = new iScroll('scrollitems');
											},
											error:  function(xhr, str){
											$('div.progressbar').hide('fast');
											alert('Возникла ошибка: ' + xhr.responseCode);},
										});
				}
				if (status!=0)
				{clearInterval(TimerTransferFrom);}				
					}
					
				});	
}	

	transferCheckTo=function ()	
{

$.ajax({
				type: 'POST',
				url: 'index.php?r=/waiter/transmitto',
				data: '',
				datatype:"json",
				success: function(data){
				var transfer = JSON.parse(data);	  
				$.each(transfer, function(i, item) {
								$.confirm({
					'title'		: 'Подтверждение передачи заказа',
					'message'	: 'Вы принимаете заказ номер '+item.check_id+' столик "'+item.table_name+'" от официанта "'+item.waiter_id+'" ?' ,
					'buttons'	: {
						'Да'	: {
							'class'	: 'blue',
							'action': function(){							
							$.ajax({
				type: 'POST',
				url: 'index.php?r=/waiter/transmitanswer',
				data: 'transfer_id='+item.transfer_id+'&status=2',
				success: ''
						}); 
						}
						},
						'Нет'	: {
							'class'	: 'gray',
							'action': function(){
							$.ajax({
				type: 'POST',
				url: 'index.php?r=/waiter/transmitanswer',
				data: 'transfer_id='+item.transfer_id+'&status=1',
				success: ''
						}); 
							}	
						}
					}
				});
				
					});
					return false;
		
						
								
				},	
	
});	

}
$('.curr_order.transmit .ready_button').live('click',function()
								{
					if ($('.curr_order.transmit .transmit_order_table .waiter').attr('id'))			
			{	waiter_id=$('.curr_order.transmit .transmit_order_table .waiter').attr('id').replace('waiter',''); }
			else waiter_id=false;
			
if (waiter_id)		
{		
					$.ajax({
				type: 'POST',
				url: 'index.php?r=/waiter/transmit',
				data: 'send_waiter_id='+waiter_id,
				success: function(html){
				alert("Запрос "+html+" на передачу заказа отправлен. Заказ будет перемещен после получения подтверждения.");
				TimerTransferFrom=setInterval(function () { transferCheckFrom(html); }, 10000);
				
					}
					
				});	
				
}	
$('.curr_order').removeClass('transmit');			
});									
	
								
								$('.curr_order.change .ready_button').live('click',function()
								{
									$('.curr_order').removeClass('change');
									
									i=0;
									item_id=[];
									count=[];	
									price=[];	
									del_order=[];	
									alreadysend=[];	
									modelist=$('.items div.mode').attr('class');
									cat_id=$('.items span.catsel').html();
									$('.curr_order .list_order .str:has(div.was_count)').each( function()
									{	
										if ($(this).attr('class')=='str send') el_alreadysend=1; else el_alreadysend=0;
										el_item_id=$(this).children('div.name').attr('id').replace('itemid','');
										el_summ=parseInt($(this).children('div.summ').html());
										el_change_count=parseInt($(this).children('div.count').html());
										el_was_count=parseInt($(this).children('div.was_count').html());
										if (el_change_count!=0) el_price=(el_summ/el_change_count); else el_price=el_summ;
										
										if (el_change_count>=0) 
											{ if(el_change_count>el_was_count)
											el_count=el_change_count-el_was_count;
											 else
											 el_count=el_was_count-el_change_count;
										}
										else
										el_count=el_was_count-el_change_count;
										
										if (el_was_count>el_change_count)
										el_del_order=-1;
										else
										el_del_order=1;
										
									item_id[i]=el_item_id;	
									count[i]=Math.abs(el_count);	
									price[i]=el_price;	
									del_order[i]=el_del_order;	
									alreadysend[i]=el_alreadysend;	
									i++;									
									});
									if(item_id.length>0)
									{
									$('div.progressbar').show();
									
									$.ajax({
											type: 'POST',
											url: 'index.php?r=/site/reloadwidget',
											data: 'widgetName=CheckMiniWidget&item_id='+item_id+'&price='+price+'&count='+count+'&del_order='+del_order+'&alreadysend='+alreadysend+'&massiv=1',
											success: function(html){											
											$('.curr_order').html(html);
											},
											error:  function(xhr, str){
											alert('Возникла ошибка: ' + xhr.responseCode);},
										});
									$.ajax({
											type: 'POST',
											url: 'index.php?r=/site/reloadwidget',
											data: 'widgetName=ItemsListWidget&mode_list='+modelist+'&cat_id='+cat_id,
											success: function(html){
											$('div.progressbar').hide('fast');
											$('.items').html(html);
											myScroll = new iScroll('scrollitems');
											},
											error:  function(xhr, str){
											$('div.progressbar').hide('fast');
											alert('Возникла ошибка: ' + xhr.responseCode);},
										});
									}
									

								});
								$('.curr_order.change .list_order .str div.esc,.curr_order.change .list_order .str div.add').live('click',function()
								{	str=$(this).parent();
									was_count=$(this).parent().children('div.was_count').html();
									count=$(this).parent().children('div.count');
									summ=$(this).parent().children('div.summ');
									price=parseInt(summ.html())/parseInt(count.html());
									
									itogo=$('.curr_order.change .summ_order .summ');
									summ_itogo=parseInt(itogo.html().replace(/[A-z\s]*[^0-9]+[A-z\s]*/g,''));
									text1_itogo=itogo.html().replace(/[0-9]+[\s\S]*/g,'');
									text2_itogo=itogo.html().replace(/[\s\S]*[0-9]+/g,'');
									
									if ($(this).attr('class')=='esc') 
									del_order=-1; else del_order=1;
									
									if(!was_count)
									{
									was_count=parseInt(count.html());
									count.after('<div class=\"was_count\">'+was_count+'</div>');
									if (was_count<0) del_order=1;
									}
									else
									{
									
									if(parseInt(was_count)<0)  del_order=1;
									}
									

									if(parseInt(count.html())+del_order==0)
									{ str.hide();}
									
									if(parseInt(count.html())+del_order>=0)
									{ 
									one_summ=price*del_order;
									count.html(parseInt(count.html())+del_order);
									if(parseInt(count.html())!=0)
									summ.html(parseInt(summ.html())+one_summ);
									summ_itogo=summ_itogo+one_summ;
									itogo.html(text1_itogo+summ_itogo+text2_itogo);
									}
									else
									{
										if (parseInt(was_count)<0)
										{
											one_summ=price*del_order;
											count.html(parseInt(count.html())+del_order);
											summ.html(parseInt(summ.html())+one_summ);
											summ_itogo=summ_itogo+one_summ;
											itogo.html(text1_itogo+summ_itogo+text2_itogo);
										}
									}
									
									
								});
						});
						
						
				// скрываем список заказов		
				$(document).on('click', '.top_buttons .orders .active .one_button',function(event){
			
					jQuery('.top_buttons .orders .list_orders').html('');
			
				$('.top_buttons .orders .active .list_orders').hide("blind",{},600, function()
				{$('.top_buttons .orders .active').removeClass().addClass('noactive'); });
				
				
				
				});
				
				//вывод списка заказов
				$(document).on('click',' .top_buttons .orders .noactive .one_button',function(){
					jQuery('.top_buttons .tables .list_tables').html('');
			
					$('.top_buttons .tables .active .list_tables').hide();
					$('.top_buttons .tables .active').removeClass().addClass('noactive'); 
					$('div.progressbar').show();
					$('.top_buttons .orders .noactive').removeClass().addClass('active');
					$('.top_buttons .orders .active .list_orders').show("blind",{},600);
						$.ajax({
							type: 'POST',
							url: 'index.php?r=/site/reloadwidget',
							data: 'widgetName=CheckMiniWidget&orders=all',
							success: function(html){
							$('div.progressbar').hide();					
							$('.top_buttons .orders .list_orders').html(html);
							},
							error:  function(xhr, str){
											$('div.progressbar').hide();
											alert('Возникла ошибка: ' + xhr.responseCode);},
						});
						
						
						return false;
						
				});
