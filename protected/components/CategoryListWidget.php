<?php
class CategoryListWidget extends CWidget
{
    

 
    public function run()
    {
        
	
			
            $this->renderContent();
      		
		
    }
 
    protected function renderContent()
    {	
			
		$criteria1_1=new CDbCriteria(array(
							'condition'=>'`t`.`parent_id`=0',
							'distinct'=>'true',						
							'order'=>'id ASC',
							'join'=>'INNER JOIN `category` as `children` ON `t`.`id` = `children`.`parent_id`
         INNER JOIN `category` as `children2` ON `children`.`id` = `children2`.`parent_id` 
    INNER JOIN `items` as `items` ON `children2`.`id` = `items`.`category_id` ',
							
						));
		$criteria1_2=new CDbCriteria(array(
							'condition'=>'`t`.`parent_id`=0',
							'distinct'=>'true',						
							'order'=>'id ASC',
							'join'=>'INNER JOIN `items` as `items2` ON `t`.`id` = `items2`.`category_id` ',
							
						));
		$criteria1_3=new CDbCriteria(array(
							'condition'=>'`t`.`parent_id`=0',
							'distinct'=>'true',						
							'order'=>'id ASC',
							'join'=>'INNER JOIN `category` as `children2` ON `t`.`id` = `children2`.`parent_id`
		INNER JOIN `items` as `items3` ON `children2`.`id` = `items3`.`category_id`',
							
						));				


		$dataProvider1_1=new CActiveDataProvider('Category',array('criteria'=>$criteria1_1, 'pagination'=>false));
		$dataProvider1_2=new CActiveDataProvider('Category',array('criteria'=>$criteria1_2, 'pagination'=>false));
		$dataProvider1_3=new CActiveDataProvider('Category',array('criteria'=>$criteria1_3, 'pagination'=>false));
		
		//сливаем вместе все массивы
		$dataProvider1=new CActiveDataProvider('Category',array('pagination'=>false));
		$mass=(array)array_merge($dataProvider1_1->getData(),$dataProvider1_2->getData(),$dataProvider1_3->getData());
		//$mass=array_unique($mass, SORT_REGULAR);
		$mass=array_map('unserialize', array_unique(array_map('serialize', $mass)));
		$dataProvider1->setData($mass);
		
		//определяем переменные
		$id_cat=isset($_POST['cat_id'])? intval($_POST['cat_id']):0;
		$level=isset($_POST['level'])? intval($_POST['level']):1;
		$cat_parent_id=isset($_POST['cat_parent_id'])? intval($_POST['cat_parent_id']):0;
			$parent_id_cat=isset($_POST['parent_id'])? intval($_POST['parent_id']):0;
			
			$dataProvider2='';
			$dataProvider3='';
			//для первого уровня категорий
			if ($id_cat!=0)
			{
				if($level==3 && $parent_id_cat!=0 && $cat_parent_id!=0)
					{
						$criteria2_2=new CDbCriteria(array(
							'condition'=>'`t`.`parent_id`= SOME (SELECT parent_id from category WHERE id='.$parent_id_cat.')',
							'distinct'=>'true',						
							'order'=>'id ASC',
							'join'=>' INNER JOIN `items` as `items` ON `t`.`id` = `items`.`category_id` ',
							
						));
						$criteria2_1=new CDbCriteria(array(
							'condition'=>'`t`.`parent_id`= SOME (SELECT parent_id from category WHERE id='.$parent_id_cat.')',
							'distinct'=>'true',						
							'order'=>'id ASC',
							'join'=>'INNER JOIN `category` as `children` ON `t`.`id` = `children`.`parent_id`
    INNER JOIN `items` as `items` ON `children`.`id` = `items`.`category_id`  ',
							
						));
						$criteria3=new CDbCriteria(array(
							'condition'=>'`t`.`parent_id`='.$parent_id_cat,	
							'distinct'=>'true',
							'order'=>'id ASC',
							'join'=>'INNER JOIN `items` as `items` ON `t`.`id` = `items`.`category_id` ',
							));
							$dataProvider3=new CActiveDataProvider('Category',array('criteria'=>$criteria3, 'pagination'=>false)); 
							$dataProvider2_1=new CActiveDataProvider('Category',array('criteria'=>$criteria2_1, 'pagination'=>false)); 
						$dataProvider2_2=new CActiveDataProvider('Category',array('criteria'=>$criteria2_2, 'pagination'=>false)); 
								$level=3;
								if ($dataProvider3->getTotalItemCount()==0) {$dataProvider3='0'; $level=2;}
					}
					else
					{
					if($parent_id_cat!=0) // для второго уровня категорий
						{ 
						$criteria2_1=new CDbCriteria(array(
							'condition'=>'`t`.`parent_id`='.$parent_id_cat,
							'distinct'=>'true',						
							'order'=>'id ASC',
							'join'=>'INNER JOIN `category` as`children` ON `t`.`id` = `children`.`parent_id`
    INNER JOIN `items` as `items` ON `children`.`id` = `items`.`category_id` ',
							
						));
						$criteria2_2=new CDbCriteria(array(
							'condition'=>'`t`.`parent_id`='.$parent_id_cat,
							'distinct'=>'true',						
							'order'=>'id ASC',
							'join'=>'INNER JOIN `items` as `items` ON `t`.`id` = `items`.`category_id` ',
							
						));
						//получаем сестринские элементы
						
						
							
						// получаем дочерние элементы, выбранной категории
						$criteria3=new CDbCriteria(array(
							'condition'=>'`t`.`parent_id`='.$id_cat,			
							'distinct'=>'true',
							'order'=>'id ASC',
							'join'=>'INNER JOIN `items` as `items` ON `t`.`id` = `items`.`category_id` ',	
							));
							$dataProvider3=new CActiveDataProvider('Category',array('criteria'=>$criteria3, 'pagination'=>false)); 
					
								$level=3;
								
									//не возвращаем никакого текста, если ничего не найдено. по умолчанию возвращает текст, если ничего не найдено
									
									if ($dataProvider3->getTotalItemCount()==0) {$dataProvider3='0'; $level=2;}
									$dataProvider2_1=new CActiveDataProvider('Category',array('criteria'=>$criteria2_1, 'pagination'=>false)); 
						$dataProvider2_2=new CActiveDataProvider('Category',array('criteria'=>$criteria2_2, 'pagination'=>false)); 
						
						}
					else
						{ 
						$criteria2_1=new CDbCriteria(array(
							'condition'=>'`t`.`parent_id`='.$id_cat,
							'distinct'=>'true',						
							'order'=>'id ASC',
							'join'=>'INNER JOIN `category` as `children` ON `t`.`id` = `children`.`parent_id`
    INNER JOIN `items` as `items` ON `children`.`id` = `items`.`category_id` ',
							
						));
						$criteria2_2=new CDbCriteria(array(
							'condition'=>'`t`.`parent_id`='.$id_cat,
							'distinct'=>'true',						
							'order'=>'id ASC',
							'join'=>'INNER JOIN `items` as `items` ON `t`.`id` = `items`.`category_id` ',
							
						));



						$level=2;
						$dataProvider2_1=new CActiveDataProvider('Category',array('criteria'=>$criteria2_1, 'pagination'=>false)); 
						$dataProvider2_2=new CActiveDataProvider('Category',array('criteria'=>$criteria2_2, 'pagination'=>false)); 
						if ($dataProvider2_1->getTotalItemCount()==0 && $dataProvider2_2->getTotalItemCount()==0 ) {$level=1;}
						//не возвращаем никакого текста, если ничего не найдено. по умолчанию возвращает текст, если ничего не найдено
						
						}
					}		
						$dataProvider2=new CActiveDataProvider('Category',array('pagination'=>false));
						$mass2=(array)array_merge($dataProvider2_1->getData(),$dataProvider2_2->getData());
		//$mass2=array_unique($mass2, SORT_REGULAR);
		$mass2=array_map('unserialize', array_unique(array_map('serialize', $mass2)));

						$dataProvider2->setData($mass2);
						
			}
			Yii::app()->clientScript->registerScriptFile("js/categorylist.js",CClientScript::POS_END);
			
				//вызываем в любом случае 1 раз, результат передается в переменную
			$dataProvider=$this->render('/category/list_categories',array(
							'dataProvider1'=>$dataProvider1, //1 уровень категорий
							'dataProvider2'=>$dataProvider2, //2 уровень категорий
							'dataProvider3'=>$dataProvider3, //3 уровень категорий
							'level'=>$level, //для 3 уровня
							'id_cat'=>$id_cat, // для определения активного css класса
							'parent_id_cat'=>$parent_id_cat) // для определения активного css класса
							);
							 
    }  

}
?>