<?php
class CheckMiniWidget extends CWidget
{
    
	public $status;
	public $orders;
	public $item_id=0; 
    public $price; 
    public $count;
	public $del_order; 
	public $divideOrder; 
	public $moveOrder; 
	public $countorder; 
	public $count_music; 
 
    public function run()
    {      
		$this->status=Yii::app()->request->getParam('status')?Yii::app()->request->getParam('status'):'';
		$this->orders=Yii::app()->request->getParam('orders')?Yii::app()->request->getParam('orders'):'';
		$this->item_id=Yii::app()->request->getParam('item_id');
		$this->price=Yii::app()->request->getParam('price');
		$this->count=Yii::app()->request->getParam('count');
		$this->del_order=Yii::app()->request->getParam('del_order');
		$this->divideOrder=Yii::app()->request->getParam('divideOrder');
		$this->moveOrder=Yii::app()->request->getParam('moveOrder');
		$this->countorder=Yii::app()->request->getParam('countorder')?Yii::app()->request->getParam('countorder'):'';
		$this->count_music=Yii::app()->request->getParam('count_music')?Yii::app()->request->getParam('count_music'):'';
	 
			Yii::app()->clientScript->registerScriptFile('js/checkminiitem.js',CClientScript::POS_END);
			if($this->item_id && $this->divideOrder!=1)
			{$this->SaveCheckMiniItem();}
			if($this->divideOrder==1)
			{$this->DivideOrder();}
			if($this->moveOrder==1)
			{$this->MoveOrder();}
			if($this->status!='')
				$this->changeCheck(); 
	if($this->orders=='changecurrent') $this->ChangeCurrentCheck();
	if ($this->count_music==1) $this->CheckMusic();
 if($this->status!='pay' && $this->orders!='all' && $this->count_music!=1) $this->renderContent();
     if ( $this->orders=='all' && $this->count_music!=1)		$this->renderContentAll();
		
    }
 
    protected function renderContent()
		{	
					
				    $menu=Yii::app()->session['menu'];		
				
				$table_id=isset($menu['table_id'])?$menu['table_id']:false;
				
				$check_id=isset($menu['check_id'])?$menu['check_id']:false;
				
				
				
				
				
				if($table_id)
				{
					$tableget=Table::model()->findByPk($table_id);
					$table_name=$tableget->name;
				if ($checks=Check::model()->findByAttributes(array('table_id'=>$table_id,'status'=>1,'user_id'=>$menu['waiter_id'])))
					{$check_id=$checks->id;
					$menu['check_id']=$check_id;
					Yii::app()->session['menu']=$menu;
					$cdate=$checks->cdate;
					}
					}
				
				if(isset($check_id) && $check_id>0 && isset($table_id))
				{	
					$checkmini=CheckMini::model()->findByAttributes(array('check_id'=>$check_id,'status'=>1));
					if(isset($checkmini->id)) 
					{	
					$CheckItemsdataProvider=new CActiveDataProvider('CheckMiniItem',
					array('criteria'=>
					array(
					"alias"=>'checkminiitem',
					"select"=>'checkminiitem.*, SUM(count) as counteritem',
					"group"=>'checkminiitem.item_id',
					"condition"=>'parent_id='.$checkmini->id,
					'order'=>'id DESC',),
					'pagination'=>false)); 
					
					$cdate=$checkmini->cdate;
					
						}
					else {$CheckItemsdataProvider=''; 
					
					}
					$checkminisend=CheckMini::model()->findAllByAttributes(array('check_id'=>$check_id,'status'=>2),array('order'=>'id DESC'));
					$checksminiIdSend='';
					foreach($checkminisend as $check)
					{$checksminiIdSend[]=$check->id;
					$cdate=$check->cdate;}
					
					if(!empty($checksminiIdSend)) 
					{	
					$CheckItemsdataProvidersend=new CActiveDataProvider('CheckMiniItem',
					array('criteria'=>
					array(
					"alias"=>'checkminiitem',
					"select"=>'checkminiitem.*, SUM(count) as counteritem',
					"group"=>'checkminiitem.item_id',
					'order'=>'id DESC',),
					'pagination'=>false)); 
					
					$CheckItemsdataProvidersend->criteria->addInCondition('parent_id', $checksminiIdSend);
					
					}
					else {$CheckItemsdataProvidersend=''; }
					if($this->item_id) {$lastitem=$this->item_id;} else {$lastitem='';}
					if ($this->countorder==1) //вид для рассчета заказа
						{	
							$service=Config::model()->findByAttributes(array('key'=>'service'));
							$service_summ=preg_replace("/([\s]*[^\d]*[\/]*[^\d]*)$/","",$service->value);
							$service_mode=preg_replace("/([0-9]{1,5}[\s]*)/","",$service->value);
							
							if (strlen($service_mode)<=1)
							{$service_mode_count='%';}
							else
							$service_mode_count='c./чел.';
							$table=Table::model()->findByPk($table_id);
							$hallname=$table->hall->name;
							
							$this->render('/checkMiniItem/countorder',array('dataProvider'=>$CheckItemsdataProvider,'dataProvider2'=>$CheckItemsdataProvidersend, 'check_id'=>$check_id,
							'table_id'=>$table_id,'cdate'=>$cdate, 'hallname'=>$hallname,'service_summ'=>$service_summ,'service_mode_count'=>$service_mode_count));
						}
					else // обычный вид
						{
							$this->render('/checkMiniItem/index',array('dataProvider'=>$CheckItemsdataProvider,'dataProvider2'=>$CheckItemsdataProvidersend, 'check_id'=>$check_id,
							'table_name'=>$table_name,'cdate'=>$cdate,'lastitem'=>$lastitem));
						}
					
					}
				
		}  
	
	   protected function renderContentAll() //выводим список всех заказов официанта
		{	
					
				    $menu=Yii::app()->session['menu'];		
				
				$waiter_id=isset($menu['waiter_id'])?$menu['waiter_id']:false;
				
				if($waiter_id)
				{ 
				$checks=Check::model()->findAllByAttributes(array('user_id'=>$waiter_id,'status'=>1),array('order'=>'id DESC'));
				
				if(isset($checks))
				{  foreach ($checks as $check)
				 { $check_id=$check->id;
					$cdate=$check->cdate;
					$table_id=$check->table_id;	
					$CheckItemsdataProvider='';
					$CheckItemsdataProvidersend='';
					
					$checkmini=CheckMini::model()->findByAttributes(array('check_id'=>$check_id,'status'=>1));
					
						if(isset($checkmini->id)) 
						{	
						$CheckItemsdataProvider=new CActiveDataProvider('CheckMiniItem',
						array('criteria'=>
						array(
						"alias"=>'checkminiitem',
						"select"=>'checkminiitem.*, SUM(count) as counteritem',
						"group"=>'checkminiitem.item_id',
						"condition"=>'parent_id='.$checkmini->id,
						'order'=>'id DESC',),
						'pagination'=>false)); 
						
						$cdate=$checkmini->cdate;
						
							}
						$checkminisend=CheckMini::model()->findAllByAttributes(array('check_id'=>$check_id,'status'=>2),array('order'=>'id DESC'));
						$checksminiIdSend='';
						foreach($checkminisend as $check)
						{$checksminiIdSend[]=$check->id;}
						
						if(!empty($checksminiIdSend)) 
						{	
						$CheckItemsdataProvidersend=new CActiveDataProvider('CheckMiniItem',
						array('criteria'=>
						array(
						"alias"=>'checkminiitem',
						"select"=>'checkminiitem.*, SUM(count) as counteritem',
						"group"=>'checkminiitem.item_id',
						'order'=>'id DESC',),
						'pagination'=>false)); 
						//$cdate=$checkminisend->cdate;
						$CheckItemsdataProvidersend->criteria->addInCondition('parent_id', $checksminiIdSend);
						
						}
						
						
						$one_check[]=array('dataProvider'=>$CheckItemsdataProvider,'dataProvider2'=>$CheckItemsdataProvidersend, 'check_id'=>$check_id,
						'table_id'=>$table_id,'cdate'=>$cdate);
						
					}	
					if (isset($menu['check_id'])) $currcheck=$menu['check_id']; else $currcheck='';
					  if(!isset($one_check)) $one_check=false;
						$this->render('/checkMiniItem/listall',array('one_check'=>$one_check,'curr_check'=>$currcheck));
						
					
						
						
				}
				}
				
		} 
	protected function ChangeCurrentCheck() //смена текущего заказа
	{	$menu=Yii::app()->session['menu'];
		$check_id=Yii::app()->request->getParam('check_id')?Yii::app()->request->getParam('check_id'):false;
		$table_id=Yii::app()->request->getParam('table_id')?Yii::app()->request->getParam('table_id'):false;
		if ($check_id==false)
		{
			$table=Table::model()->findByAttributes(array('status'=>1));
			
			if (!$table)
			{
				$table=new Table;
				$table->hall_id='1';
				$table->name="Столик";
				$table->desc="Условный столик";
				$table->status=1;
				$table->info='';
				$table->save();				
			}
				$table->status=2;
				$table->save();
				$table_id=$table->id;
				$modelCheck=new Check;
				$modelCheck->user_id=$menu['waiter_id'];
				$modelCheck->table_id=$table_id;
				$modelCheck->cdate=date('Y-m-d H:i:s',time());
				$modelCheck->info='';
				$modelCheck->status=1;
				$modelCheck->save();
				$check_id=$modelCheck->id;
			
		}
		if ($check_id && $table_id)
		{
		$menu=array_merge($menu,array('table_id'=>$table_id,'check_id'=>$check_id));
		Yii::app()->session['menu']=$menu;
		}
	}
	protected function changeCheck()
		{	 
			$menu=Yii::app()->session['menu'];				
			$check_id=isset($menu['check_id'])?$menu['check_id']:false;
			$table_id=isset($menu['table_id'])?$menu['table_id']:false;
			
			$check_id=Yii::app()->request->getParam('check_id')?Yii::app()->request->getParam('check_id'):$check_id;
			$table_id=Yii::app()->request->getParam('table_id')?Yii::app()->request->getParam('table_id'):$table_id;
			
				if($this->status=='del')
					{	$checkmini=CheckMini::model()->findByAttributes(array('check_id'=>$check_id,'status'=>1));
					
						$checkminiitem=CheckMiniItem::model()->deleteAllByAttributes(array('parent_id'=>$checkmini->id));
						$checkmini->delete();
						
					}
				elseif($this->status=='pay')
					{	
						
						
						$music=Yii::app()->request->getParam('music')?Yii::app()->request->getParam('music'):0;
						$discount=Yii::app()->request->getParam('discount')?Yii::app()->request->getParam('discount'):0;
						$people=Yii::app()->request->getParam('people')?Yii::app()->request->getParam('people'):0;
						$service=Yii::app()->request->getParam('service')?Yii::app()->request->getParam('service'):0;
						$itogo=Yii::app()->request->getParam('itogo')?Yii::app()->request->getParam('itogo'):0;
						$copy=Yii::app()->request->getParam('copy')?Yii::app()->request->getParam('copy'):0;
						
						$check=Check::model()->updateByPk($check_id,array('status'=>2,'music'=>$music,'discount'=>$discount,'peoples_count'=>$people,'service'=>$service,'itogo'=>$itogo,'cdate_close'=>date('Y-m-d H:i:s',time())));
						$checkmini=CheckMini::model()->updateAll(array('status'=>3),'check_id='.$check_id);
						
						$table=Table::model()->findByPk($table_id);
						
						if ($table)
						{ 
							if ($table->desc=="Условный столик")
							{$table->delete(); }
							else
							{
							$table->status=1;
							$table->save(); 
							}
						CheckMiniTrack::model()->updateAll(array('status'=>4),'check_id='.$check_id);
						Playstack::model()->deleteAllByAttributes(array('table_id'=>$table_id));
						Trackslist::model()->deleteAllByAttributes(array('check_id'=>$check_id,'complite'=>0));
						}
						if ($table_id==$menu['table_id'])
						{
						$menu=array_merge($menu,array('table_id'=>'','check_id'=>''));
						Yii::app()->session['menu']=$menu;
						}
						
						
					}
				else 
					{
					$checkmini=CheckMini::model()->updateAll(array('status'=>$this->status),
					'check_id='.$check_id.' AND status=1');
					}
		}
	protected function SaveCheckMiniItem()
		{	
		$alreadysend=Yii::app()->request->getParam('alreadysend');
		$massiv=Yii::app()->request->getParam('massiv');
		
		$menu=Yii::app()->session['menu'];
		if(isset($menu['check_id']) && !empty($menu['check_id'])) //если есть текущий заказ
			 {
				//ищем есть ли открытая чекушка
				$modelCheckMini=CheckMini::model()->findByAttributes(array('check_id'=>$menu['check_id'],'status'=>1));
				if(!isset($modelCheckMini->id)) //есть чекушка
				{ 
				//создаем чекушку
					$modelCheckMini=new CheckMini;
					$modelCheckMini->check_id=$menu['check_id'];
					$modelCheckMini->cdate=date('Y-m-d H:i:s',time());
					$modelCheckMini->status=1;
					$modelCheckMini->info='';
					$modelCheckMini->save();	
				}
				}
		
		
		if ($massiv==1 && $alreadysend)
		{
			$this->item_id=explode(',',$this->item_id);
			$this->count=explode(',',$this->count);
			$this->price=explode(',',$this->price);
			$this->del_order=explode(',',$this->del_order);
			$alreadysend=explode(',',$alreadysend);
			$elCount=0;
			foreach ($this->item_id as $item)
			{  
				
				if ($this->del_order[$elCount]==1) //добавление
				{	   $modelCheckMiniItem=new CheckMiniItem;
							$modelCheckMiniItem->check_id=$menu['check_id'];
							$modelCheckMiniItem->parent_id=$modelCheckMini->id;
							$modelCheckMiniItem->item_id=$item;
							$modelCheckMiniItem->count=$this->count[$elCount];
							$modelCheckMiniItem->summ=$this->price[$elCount];
							$modelCheckMiniItem->info='';
							$modelCheckMiniItem->save(); 
				}
				else //удаление
				{ 
					if ($alreadysend[$elCount]==1) //уже отправленный заказ
					{    $this->count[$elCount]=$this->count[$elCount]*$this->del_order[$elCount];
						 $modelCheckMiniItem=new CheckMiniItem;
							$modelCheckMiniItem->check_id=$menu['check_id'];
							$modelCheckMiniItem->parent_id=$modelCheckMini->id;
							$modelCheckMiniItem->item_id=$item;
							$modelCheckMiniItem->count=$this->count[$elCount];
							$modelCheckMiniItem->summ=$this->price[$elCount];
							$modelCheckMiniItem->info='';
							$modelCheckMiniItem->save();
					}
					else
					{	
						while (intval($this->count[$elCount])>0)
						{ 
						$modelCheckMiniItem=CheckMiniItem::model()->findByAttributes(
						array('item_id'=>$item,'parent_id'=>$modelCheckMini->id),'count>0');
						$countItem=$modelCheckMiniItem->count;
						if ($countItem<=$this->count[$elCount])
						{$modelCheckMiniItem->delete(); $this->count[$elCount]=$this->count[$elCount]-$countItem;}
						else
						{
						$modelCheckMiniItem->count=$countItem-$this->count[$elCount];
						$modelCheckMiniItem->save(); $this->count[$elCount]=0;
						}
						} 
					}
				}
				
				$elCount++;
			}
		}
		else
		{
		if($this->del_order) {$this->count=$this->count*$this->del_order;}
			$summ=abs($this->price);
			
			if(!isset($menu['check_id']) || empty($menu['check_id'])) //если есть текущий заказ
			 {      
					if(isset($menu['table_id']) && $menu['table_id']>0)
					{$table_id=$menu['table_id'];}
					else
					{
					$table=Table::model()->findByAttributes(array('status'=>1));
					if ($table)
					{
						$table_id=$table->id;
						$table->status=2;
						$table->save();
					}
					else
					{
						$table=new Table;
					$table->hall_id='1';
					$table->name="Столик";
					$table->desc="Условный столик";
					$table->status=2;
					$table->info='';
					$table->save();
					$table_id=$table->id;
					}
						$menu=array_merge($menu,array('table_id'=>$table_id));
						Yii::app()->session['menu']=$menu;
					}
					$modelCheck=new Check;
					$modelCheck->user_id=$menu['waiter_id'];
					$modelCheck->table_id=$table_id;
					$modelCheck->cdate=date('Y-m-d H:i:s',time());
					$modelCheck->info='';
					$modelCheck->status=1;
					$modelCheck->save();
				//записываем в сессию значение заказа
					$menu=Yii::app()->session['menu'];
					$menu=array_merge($menu,array('check_id'=>$modelCheck->primaryKey));
					Yii::app()->session['menu'] = $menu;
				//создаем чекушку
					$modelCheckMini=new CheckMini;
					$modelCheckMini->check_id=$modelCheck->primaryKey;
					$modelCheckMini->cdate=date('Y-m-d H:i:s',time());
					$modelCheckMini->status=1;
					$modelCheckMini->info='';
					$modelCheckMini->save();
				
				
			}
			
			//удаление из уже отправленного заказа
			if($alreadysend==1 && $this->del_order==-1)
			{
			$modelCheckMiniItem=new CheckMiniItem;
					$modelCheckMiniItem->check_id=$menu['check_id'];
					$modelCheckMiniItem->parent_id=$modelCheckMini->id;
					$modelCheckMiniItem->item_id=$this->item_id;
					$modelCheckMiniItem->count=$this->count;
					$modelCheckMiniItem->summ=$summ;
					$modelCheckMiniItem->info='';
					$modelCheckMiniItem->save();
			}
			elseif($this->del_order==-1)
			{
				$this->count=abs($this->count);
			while (intval($this->count)>0)
				{ 
						$modelCheckMiniItem=CheckMiniItem::model()->findByAttributes(
						array('item_id'=>$this->item_id,'parent_id'=>$modelCheckMini->id),'count>0');
						$countItem=$modelCheckMiniItem->count;
						if ($countItem<=$this->count)
						{$modelCheckMiniItem->delete(); $this->count=$this->count-$countItem;}
						else
						{
						$modelCheckMiniItem->count=$countItem-$this->count;
						$modelCheckMiniItem->save(); $this->count=0;
						}
				} 
			}
			else
			{ 
				$modelCheckMiniItem=new CheckMiniItem;
					$modelCheckMiniItem->check_id=$menu['check_id'];
					$modelCheckMiniItem->parent_id=$modelCheckMini->id;
					$modelCheckMiniItem->item_id=$this->item_id;
					$modelCheckMiniItem->count=$this->count;
					$modelCheckMiniItem->summ=$summ;
					$modelCheckMiniItem->info='';
					$modelCheckMiniItem->save();
			}
			}
		}	
		protected function DivideOrder()
		{
				$menu=Yii::app()->session['menu'];
				$divideTable=Yii::app()->request->getParam('divideTable'); //номер столика для разделения
				$statusTable=Yii::app()->request->getParam('statusTable'); //статус столика для разделения
				$countcheckTable=Yii::app()->request->getParam('countcheckTable'); //наличие неотправленных чекушек
				$check_idTable=Yii::app()->request->getParam('check_idTable'); //номер заказа столика для разделения
				$alreadysend=Yii::app()->request->getParam('alreadysend');
			
			if($statusTable==1) //создаем чек для свободного столика
			{
			if ($divideTable=='free')
			{
					$table=new Table;
					$table->hall_id='1';
					$table->name="Столик";
					$table->desc="Условный столик";
					$table->status=1;
					$table->info='';
					$table->save();
					$divideTable=$table->id;
			}
					$modelCheck=new Check;
					$modelCheck->user_id=$menu['waiter_id'];
					$modelCheck->table_id=intval($divideTable);
					$modelCheck->cdate=date('Y-m-d H:i:s',time());
					$modelCheck->info='';
					$modelCheck->status=1;
					$modelCheck->save();
					Table::model()->updateByPk(intval($divideTable),array('status'=>2));
			}
			if($countcheckTable==0)
			{
				if($check_idTable=='') $check_idTable=$modelCheck->primaryKey; 
				
				//создаем чекушку
					$modelCheckMini=new CheckMini;
					$modelCheckMini->check_id=$check_idTable;
					$modelCheckMini->cdate=date('Y-m-d H:i:s',time());
					$modelCheckMini->status=1;
					$modelCheckMini->info='';
					$modelCheckMini->save();
			}
			else
			{
			   //или находим существующую
				$modelCheckMini=CheckMini::model()->findByAttributes(array('check_id'=>$check_idTable,'status'=>1));
				
			}
			
			$modelCheckMiniSend=CheckMini::model()->findByAttributes(array('check_id'=>$check_idTable,'status'=>2));
			
			if (!$modelCheckMiniSend)
			{
			//создаем чекушку
					$modelCheckMiniSend=new CheckMini;
					$modelCheckMiniSend->check_id=$check_idTable;
					$modelCheckMiniSend->cdate=date('Y-m-d H:i:s',time());
					$modelCheckMiniSend->status=2;
					$modelCheckMiniSend->info='';
					$modelCheckMiniSend->save();
			}
			 //$modelCheckMini->primaryKey;
			 //$modelCheck->primaryKey;
			 
				//данные позиций товаров для разделения
				$this->item_id=explode(',',$this->item_id);
				$this->count=explode(',',$this->count);
				$this->price=explode(',',$this->price);
				$alreadysend=explode(',',$alreadysend);
				$elCount=0;
				
			$modelCheckMiniOld=CheckMini::model()->findByAttributes(
							array('check_id'=>$menu['check_id'],'status'=>1));
			
			
			foreach ($this->item_id as $item)
			{ $count_change=intval($this->count[$elCount]);
			
							if (intval($alreadysend[$elCount])==1)
							{
							
							//проверяем не отменен ли этот заказ
							$modelCheckMiniItemsEsc=CheckMiniItem::model()->findAllByAttributes(
							array('check_id'=>$menu['check_id'],'item_id'=>$item),'count<0');
							
								$modelCheckMiniItem=new CheckMiniItem;
							$modelCheckMiniItem->check_id=$menu['check_id'];
							$modelCheckMiniItem->parent_id=$modelCheckMiniOld->id;
							$modelCheckMiniItem->item_id=$item;
							$modelCheckMiniItem->count=$this->count[$elCount]*(-1);
							$modelCheckMiniItem->summ=$this->price[$elCount];
							$modelCheckMiniItem->info='';
							$modelCheckMiniItem->save();
							$count_change=0;
							$parentcheck=$modelCheckMiniSend->id;
							
							
							}
							else
							{ 
								while ($count_change!=0)
										{     
												$modelCheckMiniItem=CheckMiniItem::model()->findByAttributes(
												array('item_id'=>$item,'parent_id'=>$modelCheckMiniOld->id));
												
													$countItem=$modelCheckMiniItem->count;
													if (abs($countItem)<=abs($count_change))
													{
													$count_change=$count_change-$countItem;
													$modelCheckMiniItem->delete(); 
													}
													else
													{
													$modelCheckMiniItem->count=$countItem-$count_change;
													$modelCheckMiniItem->save(); $count_change=0;
													
													}
												
											
											
										
										
										} 
										$parentcheck=$modelCheckMini->id;
					}
					if ($count_change==0)
					{
							$modelCheckMiniItem=new CheckMiniItem;
							$modelCheckMiniItem->check_id=$check_idTable;
							$modelCheckMiniItem->parent_id=$parentcheck;
							$modelCheckMiniItem->item_id=$item;
							$modelCheckMiniItem->count=$this->count[$elCount];
							$modelCheckMiniItem->summ=$this->price[$elCount];
							$modelCheckMiniItem->info='';
							$modelCheckMiniItem->save();
					}
			$elCount++;
			}
		}
		

		protected function MoveOrder()
		{
				$menu=Yii::app()->session['menu'];
				$moveTable=Yii::app()->request->getParam('moveTable'); //номер столика для перемещения
				$statusTable=Yii::app()->request->getParam('statusTable'); //статус столика для перемещения
				$countcheckTable=Yii::app()->request->getParam('countcheckTable'); //наличие неотправленных чекушек
				$check_idTable=Yii::app()->request->getParam('check_idTable'); //номер заказа столика для перемещения
				
				
				if($statusTable==1) //если столик свободен, то просто перемещаем заказ
				{
					if ($moveTable=='free')
			{
					$table=new Table;
					$table->hall_id='1';
					$table->name="Столик";
					$table->desc="Условный столик";
					$table->status=1;
					$table->info='';
					$table->save();
					$moveTable=$table->id;
			}
					$modelCheck=Check::model()->findByAttributes(array('id'=>$menu['check_id'],'status'=>1));
					if( $modelCheck)
					{
					$modelCheck->table_id=$moveTable;
					$modelCheck->save();
					
					
					
					$table=Table::model()->findByPk($moveTable);
					
					
					$table->status=2;
					$table->save();
					
					
					}
				}
			   else 
			   {
					$modelCheckMiniOld=CheckMini::model()->findByAttributes(array('check_id'=>$menu['check_id'],'status'=>1)); //находим открытую старую чекушку
					$modelCheckMiniOldSend=CheckMini::model()->findAllByAttributes(array('check_id'=>$menu['check_id'],'status'=>2)); //находим закрытые чекушки
					
					if ($modelCheckMiniOld)
					{
						$modelCheckMini=CheckMini::model()->findByAttributes(array('check_id'=>$check_idTable,'status'=>1));
						if(!$modelCheckMini)
						{
							$modelCheckMiniOld->check_id=$check_idTable;
							$modelCheckMiniOld->save();
							CheckMiniItem::model()->updateAll(array('check_id'=>$check_idTable), 'parent_id='.$modelCheckMiniOld->id);
						}
						else
						{
							CheckMiniItem::model()->updateAll(array('check_id'=>$check_idTable,'parent_id'=>$modelCheckMini->id), 'parent_id='.$modelCheckMiniOld->id);
							$modelCheckMiniOld->status=3;
							$modelCheckMiniOld->save();
							
						}
					}
					
					if ($modelCheckMiniOldSend)
					{
						
						CheckMini::model()->updateAll(array('check_id'=>$check_idTable), 'check_id='.$menu['check_id'].' AND status=2');
						foreach ($modelCheckMiniOldSend as $minicheck_send)
						{
						 CheckMiniItem::model()->updateAll(array('check_id'=>$check_idTable), 'parent_id='.$minicheck_send->id);
						}
						
					}
					
					$currCheck=Check::model()->findByAttributes(array('id'=>$menu['check_id'],'status'=>1));
					$currCheck->status=2;
					$currCheck->save();
					
					$menu['check_id']=$check_idTable;
					
					
			   }
			   
			   $tablecurr=Table::model()->findByPk($menu['table_id']);
			   if ($tablecurr->desc=="Условный столик")
						{$tablecurr->delete(); }
						else
						{
					$tablecurr->status=1;
					$tablecurr->save();
				}
				$menu['table_id']=$moveTable;
			   Yii::app()->session['menu']=$menu;
			
			
		}
		protected function CheckMusic()
		{
			//цена одной песни
     $pricetrack = Config::getParam('pricefortrack');
     $check_id=Yii::app()->request->getParam('check_id')?Yii::app()->request->getParam('check_id'):false;
     //количество спетых песен
     $trackcount = Trackslist::model()->count("check_id=$check_id AND complite=1");
     
     $muzsumm = $trackcount*$pricetrack;
			echo $muzsumm;
		}
	}
?>