<?php
class ItemsListWidget extends CWidget
{
    
    
    public $selectCategory; 
    public $mode_list; 
	public $itemmodesave;

 
    public function run()
    {
         
			Yii::app()->clientScript->registerScriptFile("js/itemslist.js",CClientScript::POS_END);
	$this->mode_list=Yii::app()->request->getParam('mode_list');
	
	$this->itemmodesave=Yii::app()->request->getParam('itemmodesave')?Yii::app()->request->getParam('itemmodesave'):false;		
			
     if (!$this->itemmodesave)       $this->renderContent(); 
      else 	  $this->itemMode();
		
    }
 
    protected function renderContent()
    {		
	$menu=Yii::app()->session['menu'];
		 $waiter=Waiter::model()->findByPk($menu['waiter_id']);
		 if ($waiter && $this->mode_list=='')
		 { switch ($waiter->mode_list)
		   {
		   case '1':
		   $this->mode_list='mode mode_list_one';
		   break;
		   case '3':
		   $this->mode_list='mode mode_list_three';
		   break;
		   case '4':
		   $this->mode_list='mode mode_list_four';
		   break;
		   }
		 }
// получаем товары 
			$this->selectCategory=Yii::app()->request->getParam('cat_id');
						if(!$this->selectCategory) {$this->selectCategory=0;}
						
						
							$criteria=new CDbCriteria(array(
								'alias'=>'items',							
								'select'=>array('`items`.*', '`category`.`parent_id` as cat_cat_id', '`parents`.`parent_id` as cat_cat_cat_id'),
								'order'=>'`items`.`name` ASC',
								'with'=>'category.parents',
								
								
										));
						
				$ItemsdataProvider=new CActiveDataProvider('Items',array('criteria'=>$criteria,'pagination'=>false));
                      
        $this->render('/items/index',array('dataProvider'=>$ItemsdataProvider,'selectCategory'=>$this->selectCategory,'mode_list'=>$this->mode_list));
    }  
	protected function itemMode()
	{
		$this->render('/items/mode');
	}
}
?>