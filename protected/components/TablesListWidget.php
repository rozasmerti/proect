<?php
class TablesListWidget extends CWidget
{
   
    public $visible=0; 
    public $table_id; 
    public $check_id; 
    public $classtable; 
    public $get_check; 
    public $selectOtherTable; 

 
    public function run()
    {
		$this->visible=Yii::app()->request->getParam('visible')?Yii::app()->request->getParam('visible'):$this->visible;
		$this->get_check=Yii::app()->request->getParam('get_check');
		$this->table_id=Yii::app()->request->getParam('table_id')?Yii::app()->request->getParam('table_id'):false;
		$this->check_id=Yii::app()->request->getParam('check_id')?Yii::app()->request->getParam('check_id'):false;
		$this->classtable=Yii::app()->request->getParam('classtable')?Yii::app()->request->getParam('classtable'):false;
		$this->selectOtherTable=Yii::app()->request->getParam('selectOtherTable')?Yii::app()->request->getParam('selectOtherTable'):false;
        if($this->visible==1)
        {
			if($this->selectOtherTable==1)
			{
				$this->selectTable();
			}
			else
			{
		
				if($this->get_check>0)
					{
						$this->renderOrder();
					}
					else
					{
						if($this->table_id)
						{ 
							$this->renderOneTable();
			  
						}
						else
						$this->renderContent();
					}
			}
        }
		else
		{
				Yii::app()->clientScript->registerScriptFile("js/tableslist.js",CClientScript::POS_END); }
		
    }
 
    protected function renderContent()
    {	$menu=Yii::app()->session['menu'];
		$waiter_id=isset($menu['waiter_id'])?$menu['waiter_id']:false;
		$table_id=isset($menu['table_id'])?$menu['table_id']:false;
		$hallsData=new CActiveDataProvider('Hall');   
        $dataProvider=new CActiveDataProvider('Table');                  
        $this->render('/table/index',array('dataProvider'=>$dataProvider,'hallsData'=>$hallsData,'waiter_id'=>$waiter_id,'table_id'=>$table_id));
    } 
	protected function 	renderOrder()
	{
		
			$CheckItemsdataProvider=new CActiveDataProvider('CheckMiniItem',
						array('criteria'=>
						array(
						"alias"=>'checkminiitem',
						"select"=>'checkminiitem.item_id, checkminiitem.parent_id,  SUM(count) as counteritem',
						"group"=>'checkminiitem.item_id',
						"condition"=>'check_id='.$this->get_check,
						'order'=>'id DESC',),
						'pagination'=>false));
			if ($CheckItemsdataProvider)
			 $this->render('checkMiniItem/minicheck',array('CheckItemsdataProvider'=>$CheckItemsdataProvider));			
			
		
		
	}
	//обновляем один столик
	protected function 	renderOneTable()
	{	
	
	$col=Yii::app()->request->getParam('col')?Yii::app()->request->getParam('col'):false;
	$menu=Yii::app()->session['menu'];
		if ($this->check_id)
					{$check_id=$this->check_id;
					$menu['check_id']=$this->check_id;
					}
		if ($this->table_id && $this->classtable!='selectother' && $this->table_id>0)
					{$table_id=$this->table_id;
					if ($this->classtable=='selectnosend' || $this->classtable=='select')
					$menu['table_id']=$this->table_id;
					}
		if ($this->table_id=='free')
			{
					$table=new Table;
					$table->hall_id='1';
					$table->name="Столик";
					$table->desc="Условный столик";
					$table->status=1;
					$table->info='';
					$table->save();
					$free=1;
					$this->table_id=$table->id;
			}
		else {$free=false;}
		if(!$this->classtable || $this->classtable=='undefined')
		{ 
		if (Table::model()->updateByPk($this->table_id,array('status'=>'2'),'status=1'))
			{
				$modelCheck=new Check;
				$modelCheck->user_id=$menu['waiter_id'];
				$modelCheck->table_id=$this->table_id;
				$modelCheck->cdate=date('Y-m-d H:i:s',time());
				$modelCheck->info='';
				$modelCheck->status=1;
				$modelCheck->save();
				$menu['table_id']=$this->table_id;
				$menu['check_id']=$modelCheck->primaryKey; }
				
				}
		
		Yii::app()->session['menu']=$menu;
		if($col=='free_tables' && $free!=1)
		{ echo '';}
		else 
		{
		 if($free==1)
		 {
		 echo '<div id="tablefree" class="view">
			<div class="all">
				Столик			
				<div>Условный столик</div>
		
			</div>
		</div>';
		 }
		 else
		 {
		$dataTable=Table::model()->findByPk($this->table_id);           
        $this->render('/table/view',array('data'=>$dataTable,'check_id'=>$this->check_id,'waiter_id'=>$menu['waiter_id'],
		'table_id'=>$this->table_id));
		}
		}
		
		
	}
	protected function selectTable()
	{
	 $menu=Yii::app()->session['menu'];
		$waiter_id=isset($menu['waiter_id'])?$menu['waiter_id']:false;
		$table_id=isset($menu['table_id'])?$menu['table_id']:false;
		$hallsData=new CActiveDataProvider('Hall');   
        $dataProvider=new CActiveDataProvider('Table');                  
        $this->render('/table/selecttable',array('dataProvider'=>$dataProvider,'hallsData'=>$hallsData,'waiter_id'=>$waiter_id,'table_id'=>$table_id));
	}
}
?>