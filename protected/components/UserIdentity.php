<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
/*	public function authenticate()
	{
		$users=array(
			// username => password
			'1'=>'demo',
			'demo'=>'demo',
			'admin'=>'admin',
		);
		if(!isset($users[$this->id]))
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		else if($users[$this->id]!==$this->password)
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		else
			$this->errorCode=self::ERROR_NONE;
		return !$this->errorCode;
	} */
	// private $__id;

 
    public function authenticate()
    {
        $user_id=$this->id;
       $user=Waiter::model()->findByPk(array($user_id));
        if($user===null)
            $this->errorCode=$this->id;
        else if(!$user->validatePassword($this->password))
            $this->errorCode=self::ERROR_PASSWORD_INVALID;
        else
        {
		$menu = Yii::app()->session['menu'];
  
  $menu['waiter_id'] = $user->id;
  $menu['waiter_name'] = $user->name;
  
  Yii::app()->session['menu'] = $menu;
            //$this->__id=$user_id;
            $this->username=$user->name;
            $this->errorCode=self::ERROR_NONE;
        }
        return $this->errorCode==self::ERROR_NONE;
    }
 
/* public function getIds()
    {
        return $this->__id;
    }
*/

}