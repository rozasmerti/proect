<?php
/* @var $this CheckMiniItemController */
/* @var $dataProvider CActiveDataProvider */

$summ=0;
?>

<div class="order_table">
			<div class="info_str">
				<div class="num_order">
					&#8470;<?php echo $check_id; ?>
				</div>
<div class="hall">Зал : <?php echo $hallname?> </div>
<div class="time">Время : <?php echo date("H:i",strtotime($cdate))?> </div>
				<div class="num_table">
				<?php echo $table_id; ?>
				</div>
				<div class="esc_button">Отмена</div>
			</div>
<div class="right_col">
	<div class="count_discount">
	<span>Указать скидку</span>
		<div class="disscounts">
			 <div>НЕТ</div>
			 <div class="line"></div>
			 <div>15%</div>
			 <div class="line"></div>
			 <div>20%</div>
			 <div class="line"></div>
			 <div>40%</div>
			 <div class="line"></div>
			 <div>50%</div>
			 <div class="line"></div>
			 <div>75%</div>
			 <div class="line"></div>
			 <div>100%</div>
		</div>
	</div>
	<?php if ($service_mode_count!='%')
	{ ?>
	<div class="count_people">
	<span>Количество людей</span>
	<div class="peoples">
			 <div class="first">НЕТ</div>
			 <div class="line"></div>
	<div class="outercount">
		<div class="listpeoples">
			 <div>1
			 <span>чел.</span>
			 </div>
			 <div class="line"></div>
			 <div>2
			 <span>чел.</span>
			 </div>
			 <div class="line"></div>
			 <div>3
			 <span>чел.</span>
			 </div>
			 <div class="line"></div>
			 <div>4
			 <span>чел.</span>
			 </div>
			 <div class="line"></div>
			 <div>5
			 <span>чел.</span>
			 </div>
			 <div class="line"></div>
			 <div>6
			 <span>чел.</span>
			 </div>
			 <div class="line"></div>
			  <div>7
			 <span>чел.</span>
			 </div>
			 <div class="line"></div>
			 <div>8
			 <span>чел.</span>
			 </div>
			 <div class="line"></div>
			 <div>9
			 <span>чел.</span>
			 </div>
			 <div class="line"></div>
			 <div>10
			 <span>чел.</span>
			 </div>
			 <div class="line"></div>
			 <div>11
			 <span>чел.</span>
			 </div>
			 <div class="line"></div>
			 <div>12
			 <span>чел.</span>
			 </div>
			 <div class="line"></div>
		</div>
	</div> 
			 <div class="arrow"></div>
		</div>
	</div>
	<?php } ?>
	<div class="count_params">
		<div class="count_music">
			<span>Считать музыку</span>
			<div class="param_mode" id="count_music">
				<span>Нет</span>
			</div>
		</div>
		<div class="copy">
			<span>Сделать копию чека</span>
			<div class="param_mode" id="copy">
				<span>Нет</span>
			</div>
		</div>
		<div class="count_service">
			<span>Считать обслуживание</span>
			<div class="param_mode"  id="count_service">
				<span>Нет</span>
			</div>
		</div>
	</div>
	<div class="print" id="go">
	Напечатать чек
	<span>Перейти к другим заказам</span>
	</div>
	<div class="print" id="exit">
	Напечатать чек
	<span>Выйти из системы</span>
	</div>
</div>
<div class="left_col">
		<div class="list_order">


		<?php 
		if($dataProvider) 
		{
		foreach ($dataProvider->getData() as $data)
		{ 
			if($data->counteritem!=0) {
		?>

						<div class="str">
						
						<div class="name" id="itemid<?php echo CHtml::encode($data->item_id); ?>"><?php echo CHtml::encode($data->item->name); ?></div>
						<div class="count"><?php echo CHtml::encode($data->counteritem); ?></div>
						<div class="summ"><?php echo CHtml::encode($data->counteritem*$data->summ); ?></div>
						</div>
		<?php

		$summ+=($data->counteritem*$data->summ);
		}
		}

		} ?>
		 

		<?php 
		if($dataProvider2) 
		{ 
			if ($dataProvider)
			{
		?>
		<div class="str">&nbsp;</div>
		<?php
			}
		foreach ($dataProvider2->getData() as $data)
		{  
			if($data->counteritem>0) {
		?>

						<div class="str send">
						
						<div class="name" id="itemid<?php echo CHtml::encode($data->item_id); ?>"><?php echo CHtml::encode($data->item->name); ?></div>
						<div class="count"><?php echo CHtml::encode($data->counteritem); ?></div>
						<div class="summ"><?php echo CHtml::encode($data->counteritem*$data->summ); ?></div>
						
						</div>
		<?php

		$summ+=($data->counteritem*$data->summ);
		}
		}
		}
		 ?>

		</div>
	<div class="bottom_block">
	<div class="count_peoples">
	Кол-во посетит.
	<span></span>
	</div>
	<div class="counters_name_block">
		<div class="summ_order">
						<div class="summ"><?php if(isset($summ)) { ?>Итого : <?php } ?> </div>
					</div>
			<div class="service_order">
						Обслуживание : <br>
						(<span class="summservice"><?php echo $service_summ?></span>&nbsp;<span class="modeservice"><?php echo $service_mode_count?></span>)
					</div>
			<div class="music">
						Музыка : 
					</div>
			<div class="discount">
						<div class="info_part">Скидка <span></span>: </div>
					</div>
	</div>
	<div class="counters_summ">			
			<div class="summ_order">
						<div class="summ"><?php if(isset($summ)) { ?><span><?php echo $summ?></span> с. <?php } ?> </div>
					</div>
			<div class="service_order">
						<span></span> с.
					</div>
			<div class="music">
						<span></span> с.
					</div>
			<div class="discount">
						 <span></span> с.
					</div>
	</div>
	
	</div>
<div class="summ_order_all">
						<div class="summ"><?php if(isset($summ)) { ?>Итого : <span><?php echo $summ?></span> с. <?php } ?> </div>
					</div>	
		</div>
		
		
