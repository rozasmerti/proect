<?php
/* @var $this CheckMiniItemController */
/* @var $dataProvider CActiveDataProvider */

$summ=0;
?>
<div class="mode_order">
		<div class="change_order current">Заказ</div>
		<div class="divide_order">Разделить</div>
		<div class="transmit_order">Передать</div>
		<div class="move_order">Перенести</div>
	</div>
<div class="order_table">
			<div class="info_str">
				<div class="num_order">
					&#8470;<?php echo $check_id; ?>
				</div>
				<div class="num_table">
				<?php echo $table_name; ?>
				</div>
				<div class="modename">изменение заказа...</div>
				<div class="change_button">
				Изменить
				</div>
				<div class="ready_button">Готово</div>
			</div>
<div class="info_order">Текущий заказ <div class="num_table">
				<?php echo $table_name; ?>
				</div>
				</div>
<div class="info_order_new">Новый заказ <div class="num_table">			
				
				<div class="select_table">Выбрать</div></div>
				</div>
<div class="list_order">


<?php 
if($dataProvider) 
{
foreach ($dataProvider->getData() as $data)
{ 
	if($data->counteritem!=0) {
?>

				<div class="str<?php if ($lastitem==$data->item_id) echo ' current'; ?>" >
				<div class="esc">Отмена</div>
				<div class="name" id="itemid<?php echo CHtml::encode($data->item_id); ?>"><?php echo CHtml::encode($data->item->name); ?></div>
				<div class="count"><?php echo CHtml::encode($data->counteritem); ?></div>
				<div class="summ"><?php echo CHtml::encode($data->counteritem*$data->summ); ?></div>
				<div class="add">Добавить</div>
				<div class="str_right"></div>
				</div>
<?php

$summ+=($data->counteritem*$data->summ);
}
}

} ?>
 

<?php 
if($dataProvider2) 
{ 
	if ($dataProvider)
	{
?>
<div class="str">&nbsp;</div>
<?php
	}
foreach ($dataProvider2->getData() as $data)
{  
	if($data->counteritem>0) {
?>

				<div class="str send">
				<div class="esc">отмена</div>
				<div class="name" id="itemid<?php echo CHtml::encode($data->item_id); ?>"><?php echo CHtml::encode($data->item->name); ?></div>
				<div class="count"><?php echo CHtml::encode($data->counteritem); ?></div>
				<div class="summ"><?php echo CHtml::encode($data->counteritem*$data->summ); ?></div>
				<div class="add">добавить</div>
				<div class="str_right"></div>
				</div>
<?php

$summ+=($data->counteritem*$data->summ);
}
}
}
 ?>

</div>
<div class="list_order_new"></div>
	<div class="summ_order">
				<div class="time">Время : <?php echo date("H:i",strtotime($cdate))?> </div>
				<div class="summ"><?php if(isset($summ)) { ?>Итого : <?php echo $summ?> с. <?php } ?> </div>
			</div>
	<div class="move_order_table">Перенести за столик: <div class="num_table">			
				
				<div class="select_table">Выбрать</div></div>
				</div>
	<div class="transmit_order_table">Передать официанту: <div class="waiter">			
				
				<div class="select_waiter">Выбрать</div></div>
				</div>			
				
		</div>
		
		<div class="buttons_order">
		<?php if ($dataProvider) {?>
			<div class="ok"></div>
			<div class="esc"></div>
			<?php } else {?>
			<div class="calc">Рассчитать</div>
			<?php } ?>
		</div>