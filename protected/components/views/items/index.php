<div>
<span class="catsel"><?php echo $selectCategory?></span>
<?php 
if ($mode_list=='') $mode_list='mode mode_list_one';
//echo $activeItem;
$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'/items/_view',
	'itemsCssClass'=>$mode_list,
	'enablePagination'=>false,
	'summaryText'=>'',
	'viewData'=>array('selectCategory'=>$selectCategory)
)); ?>
</div>