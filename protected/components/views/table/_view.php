<?php
/* @var $this TableController */
/* @var $model Table */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	
	<?php 
	if($data->status==1)
	{
	echo CHtml::Link(CHtml::encode($data->id),'#', array('id'=>'table'.$data->id)); 
	}
	else 
	{echo CHtml::encode($data->id);}
	?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hall_id')); ?>:</b>
	<?php echo CHtml::encode($data->hall_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('desc')); ?>:</b>
	<?php echo CHtml::encode($data->desc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status_name->name); ?>
	<br />




</div>