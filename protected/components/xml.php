<?php

class XML implements ArrayAccess, IteratorAggregate, Countable 
	{
		//взято отсюда - http://habrahabr.ru/post/30353/
		private $pointer;
		private $tagName;
		private $attributes = array();
		private $cdata;
		private $parent;
		private $childs = array();

		public function __construct($data) 
			{
				if (is_array($data)) 
					{
						list($this->tagName, $this->attributes) = $data;

					} 
				else if (is_string($data))
					{
						$this->parse($data);
					}
			}
		public function __toString() 
			{
				return $this->cdata;
			}
		public function __get($name) 
			{
				if (isset($this->childs[$name])) 
					{
						if (count($this->childs[$name]) == 1) return $this->childs[$name][0];else return $this->childs[$name];
					}
				throw new Exception("UFO steals [$name]!");
			}
		public function offsetGet($offset) 
			{
				if (isset($this->attributes[$offset])) return $this->attributes[$offset];

                throw new Exception("Holy cow! There is'nt [$offset] attribute!");
			}
		public function offsetExists($offset) 
			{
				return isset($this->attributes[$offset]);
			}
		public function offsetSet($offset, $value) { return; }
		public function offsetUnset($offset) { return; }

		public function count() 
			{
				if ($this->parent != null) return count($this->parent->childs[$this->tagName]);
				return 1;
			}
		public function getIterator() 
			{
				if ($this->parent != null) return new ArrayIterator($this->parent->childs[$this->tagName]);
				return new ArrayIterator(array($this));
			}
		public function getAttributes() 
			{
				return $this->attributes;
			}
		public function appendChild($tag, $attributes) 
			{
				$element = new XML(array($tag, $attributes));
				$element->setParent($this);
				$this->childs[$tag][] = $element;
				return $element;
			}
		public function setParent(XML $parent) 
			{
				$this->parent =& $parent;
			}
		public function getParent() 
			{
				return $this->parent;
			}
		public function setCData($cdata) 
			{
				$this->cdata = $cdata;
			}
		private function parse($data) 
			{
				$this->pointer =& $this;
				$parser = xml_parser_create();
				xml_set_object($parser, $this);
				xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, false);
				xml_set_element_handler($parser, "tag_open", "tag_close");
				xml_set_character_data_handler($parser, "cdata");
				xml_parse($parser, $data);
			}
		private function tag_open($parser, $tag, $attributes) 
			{
			//var_dump($attributes);
				$this->pointer = $this->pointer->appendChild($tag, $attributes);
			}
		private function cdata($parser, $cdata) 
			{
				$this->pointer->setCData($cdata);
			}
		private function tag_close($parser, $tag) 
			{
				$this->pointer = $this->pointer->getParent();
			}
	}

?>