<?php

class ImportController extends Controller
{
	public function actionIndex()
		{
			$data = implode(file('upload/menu.XML'));
			$arr = new XML($data);
			//print_r($arr->XML->officiant);
			//добавление официантов
			foreach($arr->XML->officiant as $item)
				{
					$name = $item->name.'';
					$id = $item->id.'';
					$password = $item->password.'';
					$salt = $this->generateSalt();
					$pass = md5($salt.$password);
					$waiter = new Waiter;
					
					$test = $waiter->model()->findByPk($id);
					
					if (isset($test->id))
						{
							$waiter->model()->updateByPk($id,array('name'=>$name,'password'=>$pass,'salt'=>$salt));
						}
					else
						{
							$waiter->id = $id;
							$waiter->name = $name;
							$waiter->password = $pass;
							$waiter->salt = $salt;
							$waiter->status = 1;
							$waiter->save(false);
						}
				}
			//добавление залов
			foreach($arr->XML->Zali->Zal as $item)
				{
					$id = $item->id.'';
					$name = $item->name.'';
					$hall = new Hall;
					
					$test = $hall->model()->findByPk($id);
					
					if ($test->id>0)
						{
							$hall->model()->updateByPk($id,array('name'=>$name));
						}
					else
						{
							$hall->id = $id;
							$hall->name = $name;
							$hall->floor_id = 1;
							$hall->save(false);
						}
				}
			//добавление подкатегорий
			foreach($arr->XML->category as $item)
				{
					$name = $item->name.'';
					$id = $item->id.'';
					$parent_id = $item->myPlace.'';
					$category = new Category;
					
					$test = $category->model()->findByPk($id);
					
					if ($test->id>0)
						{
							$category->model()->updateByPk($id,array('name'=>$name,'parent_id'=>$parent_id));
						}
					else
						{
							$category->id = $id;
							$category->name = $name;
							$category->parent_id = $parent_id;
							$category->image = '';
							$category->info = '';
							$category->save(false);
						}
				}
			//добавление категорий нулевого уровня
			foreach($arr->XML->place as $item)
				{
					$name = $item->name.'';
					$id = $item->id.'';
					$category = new Category;
					
					$test = $category->model()->findByPk($id);
					
					if ($test->id>0)
						{
							$category->model()->updateByPk($id,array('name'=>$name,'parent_id'=>0));
						}
					else
						{
							$category->id = $id;
							$category->name = $name;
							$category->parent_id = 0;
							$category->image = '';
							$category->info = '';
							$category->save(false);
						}
				}
			//добавление продуктов
			foreach($arr->XML->item as $item)
				{
					$nameInMenu = $item->nameInMenu.'';
					$nameInOrder = $item->nameInOrder.'';
					$myCategory = $item->myCategory.'';
					$price = $item->price.'';
					$description = '';//$item->description.'';
					$status = $item->status.'';
					$id = $item->id.'';
					$time = $item->time.'';
					
					$items = new Items;
					
					$test = $items->model()->findByPk($id);
					
					if ($test->id>0)
						{
							$items->model()->updateByPk($id,array('name'=>$nameInMenu,'category_id'=>$myCategory,'price'=>$price,'full_desc'=>$description));
						}
					else
						{
							$items->id = $id;
							$items->name = $nameInMenu;
							$items->parent_id = 0;
							$items->category_id = $myCategory;
							$items->art = $id;
							$items->price = $price;
							$items->full_desc = $description;
							$items->art = $id;
							$items->short_desc = '';
							$items->photo = '';
							$items->portion = '1';
							$items->measure = '1';
							$items->info = '';
							$items->save(false);
						}
				}
			//добавление процентов
			foreach($arr->XML->procents as $item)
				{
					$name = $item->name.'';
					$id = $item->id.'';
					$procent = $item->procent.'';
					$procents = new Procents;
					
					$test = $procents->model()->findByPk($id);
					
					if ($test->name)
						{
							$procents->model()->updateByPk($id,array('name'=>$name,'procent'=>$procent));
						}
					else
						{
							$procents->id = $id;
							$procents->name = $name;
							$procents->procent = $procent;
							$procents->save();
						}
				}
			/*foreach($arr->XML->item as $item)
				{
					echo $item->id.' - '.$item->nameInMenu.' - '.$item->nameInOrder.' - '.
					$item->myCategory.' - '.$item->price.' - '.$item->status.' - '.$item->time.'<br>';
				}*/
			
			$this->renderPartial('index');
		}
	function generateSalt() 
		{
			$salt = '';
			$length = rand(5,10); // длина соли (от 5 до 10 сомволов)
			for($i=0; $i<$length; $i++) 
				{
				 $salt .= chr(rand(33,126)); // символ из ASCII-table
				}
			return $salt;
		}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}