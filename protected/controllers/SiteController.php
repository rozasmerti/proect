<?php

class SiteController extends Controller
{ 
	/**
	
	 * Declares class-based actions.
	 */
	 public $layout='//layouts/shablon_auth';
	   public function filters() {
        return array(
            'ajaxOnly + reloadwidget',
        );
    }
	
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{    
	Yii::app()->clientScript->registerScriptFile('js/keyboard.js',CClientScript::POS_END); 
if(!Yii::app()->request->isAjaxRequest) 
	{Yii::app()->clientScript->registerScriptFile("/js/waiterauth.js",CClientScript::POS_HEAD);
	
	}

			if(Yii::app()->user->isGuest)
			{
			
			CController::forward('/waiter/index');
 
			}
		else 
		{   			
	
			CController::forward('/site/authindex');
			}
	}
public function actionAuthindex()
	{  	 $visibleTableListWidget=1;	
		Yii::app()->getClientScript()->registerCoreScript( 'jquery.ui' );
$menu=Yii::app()->session['menu'];

$transfer=Transfer::model()->findAllByAttributes(array('waiter_id'=>$menu['waiter_id']),'status!=3');
foreach ($transfer as $transfet_one)
{
Yii::app()->clientScript->registerScript('transmit'.rand(), "
								jQuery(function($) {
								TimerTransferFrom=setInterval(function () { transferCheckFrom(".$transfet_one->id."); }, 10000);
								});",CClientScript::POS_END);
}
								
	if(Yii::app()->request->isAjaxRequest) 
		{

			//убираем загрузку лишних скриптов
			$cs=Yii::app()->clientScript;
			$cs->scriptMap=array(
								'jquery.js'=>false,);
		}
		
		//узнаем имя авторизованного пользователя
        $username=Yii::app()->user->name;
		
		
			
			//если это аякс запрос и при этом не пришедший с авторизации
		if (Yii::app()->request->isAjaxRequest && !isset($_POST['Waiter']))
		{ 
			
			//убираем загрузку лишних скриптов, они уже были загружены в первый раз
			$cs=Yii::app()->clientScript;
			$cs->scriptMap=array(
				'jquery.js'=>false,
				'jquery.bbq.js'=>false,
				'jquery.ba-bbq.js'=>false,
				'jquery.yiilistview.js'=>false,
			);		
		
		}			

			Yii::app()->clientScript->registerScript('modeOrder', "
								jQuery(function($) {
								 $('.mode_order .name_mode').slider({
								 min: 0,
								max: 100,
								step: 10,
								animate: '10',
								  change: function(event, ui) {
								  valuestep=$(this ).slider( 'option', 'value' );
								  
								  el=$('.mode_order  .ui-slider');
								  
								  
									  if(valuestep==100)
									  {
										
										$(el).switchClass( 'name_mode', 'name_mode2', 100 ); 
										$(el).children('span').html('Отмена');
										$('.mode_order a:eq(1)').removeClass( 'cancel'); 
										$('.mode_order a:eq(2)').addClass('cancel');
										$('.items').addClass('noactive');
											
									  }
									  if(valuestep==0)
									  {
									  $(el).switchClass( 'name_mode2', 'name_mode', 100 );
									 $(el).children('span').html('Заказ'); 
									 $('.mode_order a:eq(2)').removeClass( 'cancel'); 
									 $('.mode_order a:eq(1)').addClass('cancel'); 
									 $('.items').removeClass('noactive');
									  }
								  },
							 
						 });
												$( '.mode_order span.links > a' ).click(function() {
								$('.mode_order  .ui-slider').slider( 'value', 0 );
								return false;
								});
								$( '.mode_order span.links a:nth-child(2)' ).click(function() {
								$('.mode_order  .ui-slider ').slider( 'value', 100 );
								return false;
									});
						});",CClientScript::POS_END);
						$menu=Yii::app()->session['menu'];
						 $waiter=Waiter::model()->findByPk($menu['waiter_id']);
						 $listmode='list_one';
						 if (!$waiter->mode_list) $waiter->mode_list=3;
						 if ($waiter)
						 { switch ($waiter->mode_list)
						   {
						   case '3':
						   $listmode='list_three';
						   break;
						   case '4':
						   $listmode='list_four';
						   break;
						   }
						}
			//если пришли с формы авторизации, то генерим общее новое содержимое				
					if (isset($_POST['Waiter'])) //не генерим полностью страницу, если аякс запрос с формы
					{
					$this->renderPartial('/site/index_auth',array(
									'username'=>$username,
									'visibleTableListWidget'=>$visibleTableListWidget, 'listmode'=>$listmode),
									false,true);
					}
					else
					{
					$this->render('/site/index_auth',array(
									'username'=>$username,
									'visibleTableListWidget'=>$visibleTableListWidget, 'listmode'=>$listmode),
									false,true);
					}				
			
		
		
		
	}
	//функция управления всеми виджетами
	public function actionReloadwidget()
	{
	$widgetName=Yii::app()->request->getParam('widgetName');
	
	
	if ($widgetName)
		{ 
		   
		$this->widget('application.components.'.$widgetName);
		   
		   }
		

	}
	//изменяем любимый режим списка пользователя
	public function actionModelist()
	{
		//if (isset(Yii::app()->request->getParam('mode_list'))
	$mode_list=Yii::app()->request->getParam('mode_list');
	//else $mode_list='list_one';
	 if ($mode_list)
	 {
		switch ($mode_list)
		{
		case 'list_one':
		$mode=1;
		break;
		case 'list_three':
		$mode=3;
		break;
		case 'list_four':
		$mode=4;
		break;
		default:
		$mode=1;
		break;
		}
		echo $mode;
		$menu=Yii::app()->session['menu'];
		 $waiter=Waiter::model()->updateByPk($menu['waiter_id'],array('mode_list'=>$mode));
		
	 }
	}
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		echo Yii::app()->user->id;
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->session->destroy();
		Yii::app()->user->logout();
		//CController::forward('/site/index');
		Yii::app()->request->redirect('/', true);
		
	}
}
