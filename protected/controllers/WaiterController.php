<?php

class WaiterController extends Controller
{
//public $layout='//layouts/shablon_noauth';
 public function filters() {
        return array(
            'ajaxOnly + list',
            'ajaxOnly + login',
            'ajaxOnly + transmit',
        );
    }
	public function actionIndex()
	{

	//если пользователь не авторизован, то выдаем список официантов
		if(Yii::app()->user->isGuest)
			{
			
			$model=new Waiter;
			
$dataProvider=new CActiveDataProvider('Waiter', array(
    'criteria'=>array(
        'condition'=>'status=1',		
        'order'=>'name ASC'),
		 'pagination'=>false));
$this->render('list_waiter',array(
			'dataProvider'=>$dataProvider,
			'model'=>$model,
		));
 // print_r ($dataProvider->getItemCount());  will return a list of Post objects
 
			}
		else {
			CController::forward('/site/index');
	}
	
		// display the login form

}
public function actionList()
{
$menu=Yii::app()->session['menu'];
$model=new Waiter;
$dataProvider=new CActiveDataProvider('Waiter', array(
    'criteria'=>array(
        'condition'=>'status=1 AND role=1',		
        'order'=>'name ASC'),
		 'pagination'=>false));
		 
$this->renderPartial('list_waiterajax',array(
			'dataProvider'=>$dataProvider,
			'waiter_id'=>$menu['waiter_id'],
			'model'=>$model,
		));
}
//функция передачи заказа другому официанту
public function actionTransmit()
{
$menu=Yii::app()->session['menu'];
$send_waiter_id=Yii::app()->request->getParam('send_waiter_id')?Yii::app()->request->getParam('send_waiter_id'):'';
$transfer=new Transfer;
					$transfer->waiter_id=$menu['waiter_id'];
					$transfer->send_waiter_id=$send_waiter_id;
					$transfer->check_id=$menu['check_id'];
					$transfer->save();
					echo $transfer->id;
}

//функция уведомления о статусе передачи заказа 
public function actionTransmitfrom()
{
$menu=Yii::app()->session['menu'];
$transfer_id=Yii::app()->request->getParam('transfer_id')?Yii::app()->request->getParam('transfer_id'):'';
$transfer=Transfer::model()->findByPk($transfer_id);
	if ($transfer)
	{
		
		if ($transfer->status==2)
		{
		$check=Check::model()->findByPk($transfer->check_id);
		$check->user_id=$transfer->send_waiter_id;
		$check->save();
		$menu['check_id']='';
		$menu['table_id']='';
		Yii::app()->session['menu']=$menu;
		}
		echo $transfer->status;
		if ($transfer->status!=0)
		{
			$transfer->status=3;
			$transfer->save();
		}
	
	}
}

public function actionTransmitto() //функция проверки передаваемых заказов
{
$menu=Yii::app()->session['menu'];

$transfer=Transfer::model()->findAllByAttributes(array('send_waiter_id'=>$menu['waiter_id'],'status'=>0));
	if ($transfer)
	{
		$data = array();
		foreach ($transfer as $one_item)
		{
		 $waiter=Waiter::model()->findByPk($one_item->waiter_id);
		 $check=Check::model()->findByPk($one_item->check_id);
		 $table=Table::model()->findByPk($check->table_id);
		 $data[]=array("waiter_id"=>$waiter->name,"table_name"=>$table->name,"check_id"=>$one_item->check_id,"transfer_id"=>$one_item->id);
		}
	 
	}
	else $data =0;
	echo CJSON::encode($data);
}
public function actionTransmitanswer() //функция обработки ответа о передаче заказа
{
$transfer_id=Yii::app()->request->getParam('transfer_id')?Yii::app()->request->getParam('transfer_id'):'';
$status=Yii::app()->request->getParam('status')?Yii::app()->request->getParam('status'):'';
$transfer=Transfer::model()->findByPk($transfer_id);
$transfer->status=$status;
$transfer->save();

}



	public function actionLogin()
	{ 
	if (Yii::app()->request->isAjaxRequest) {
 
	$cs=Yii::app()->clientScript;
$cs->scriptMap=array(
    'jquery.js'=>false,
    ); }
	
	
		
		$model=new Waiter;
	
	

if (isset($_POST['waiter_id'])) $_POST['Waiter']['id']=$_POST['waiter_id'];
if (isset($_POST['waiter_name'])) { $name_waiter=$_POST['waiter_name']; $_POST['Waiter']['name']=$name_waiter; }
else {if($_POST['Waiter']['name']) $name_waiter=$_POST['Waiter']['name']; else $name_waiter='';}
		// collect user input data
		if(isset($_POST['Waiter']))
		{
			$model->attributes=$_POST['Waiter'];
			// validate user input and redirect to the previous page if valid
			
			if($model->validate() && $model->login())
				//$this->redirect(Yii::app()->user->returnUrl);
				
				//$this->render('index');
				CController::forward('/waiter/index');
				
				
		}
		// display the login form
		
		$this->renderPartial('/site/login',array('model'=>$model, 'name_waiter'=>$name_waiter,),false,true);
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}