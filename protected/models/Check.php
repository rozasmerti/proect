<?php

/**
 * This is the model class for table "check".
 *
 * The followings are the available columns in table 'check':
 * @property integer $id
 * @property string $cdate
 * @property integer $user_id
 * @property integer $table_id
 * @property integer $status
 * @property string $info
 */
class Check extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Check the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'check';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	 public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user_id, table_id, status', 'required'),
            array('user_id, table_id, status, discount, peoples_count', 'numerical', 'integerOnly'=>true),
            array('service, music, itogo', 'numerical'),
            array('cdate, cdate_close', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, cdate, user_id, table_id, status, info, cdate_close, service, music, discount, peoples_count, itogo', 'safe', 'on'=>'search'),
        );
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'waiter' => array(self::BELONGS_TO, 'Waiter', 'user_id'),
		'table' => array(self::BELONGS_TO, 'Table', 'table_id'),
		'status_name' => array(self::BELONGS_TO, 'CheckStatus', 'status'),
		'checksmini' => array(self::HAS_MANY, 'CheckMini', 'check_id', 'index'=>'id'),
		'checksminicount' => array(self::STAT, 'CheckMini', 'check_id','condition'=>'`status`=:status','params'=>array('status'=>1)),
		'checksminicountsend' => array(self::STAT, 'CheckMini', 'check_id','condition'=>'`status`=:status','params'=>array('status'=>2)),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'cdate' => 'Дата',
			'user_id' => 'Официант',
			'table_id' => 'Столик',
			'status' => 'Статус',
			'info' => 'Информация',
			'cdate_close' => 'Дата закрытия',
			'service' => 'Обслуживание',
			'music' => 'Музыка',
			'discount' => 'Скидка',
			'peoples_count' => 'Кол-во посетителей',
			'itogo' => 'Итого',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('cdate',$this->cdate,true);
        $criteria->compare('user_id',$this->user_id);
        $criteria->compare('table_id',$this->table_id);
        $criteria->compare('status',$this->status);
        $criteria->compare('info',$this->info,true);
        $criteria->compare('cdate_close',$this->cdate_close,true);
        $criteria->compare('service',$this->service);
        $criteria->compare('music',$this->music);
        $criteria->compare('discount',$this->discount);
        $criteria->compare('peoples_count',$this->peoples_count);
        $criteria->compare('itogo',$this->itogo);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

}