<?php

/**
 * This is the model class for table "check_mini".
 *
 * The followings are the available columns in table 'check_mini':
 * @property integer $id
 * @property integer $check_id
 * @property string $cdate
 * @property integer $status
 * @property string $info
 */
class CheckMini extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CheckMini the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'check_mini';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('check_id, status', 'required'),
			array('check_id, status', 'numerical', 'integerOnly'=>true),
			array('cdate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, check_id, cdate, status, info', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'check_id' => array(self::BELONGS_TO, 'Check', 'check_id'),
		'status_name' => array(self::BELONGS_TO, 'CheckMiniStatus', 'status'),
		'checkminiitems' => array(self::HAS_MANY, 'CheckMiniItem', 'parent_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'check_id' => 'Чек',
			'cdate' => 'Дата создания',
			'status' => 'Статус',
			'info' => 'Информация',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('check_id',$this->check_id);
		$criteria->compare('cdate',$this->cdate,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('info',$this->info,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}