<?php

/**
 * This is the model class for table "check_mini_item".
 *
 * The followings are the available columns in table 'check_mini_item':
 * @property integer $id
 * @property integer $parent_id
 * @property integer $item_id
 * @property integer $count
 * @property double $summ
 * @property string $info
 */
class CheckMiniItem extends CActiveRecord
{
public $counteritem;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CheckMiniItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'check_mini_item';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('parent_id, item_id, count, summ', 'required'),
			array('parent_id, item_id, count', 'numerical', 'integerOnly'=>true),
			array('summ', 'numerical'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, parent_id, item_id, count, summ, info', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'checkmini' => array(self::BELONGS_TO, 'CheckMini', 'parent_id'),
		'item' => array(self::BELONGS_TO, 'Items', 'item_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'parent_id' => 'Чекушка',
			'item_id' => 'Товар',
			'count' => 'Количество',
			'summ' => 'Сумма',
			'info' => 'Информация',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('item_id',$this->item_id);
		$criteria->compare('count',$this->count);
		$criteria->compare('summ',$this->summ);
		$criteria->compare('info',$this->info,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}