<?php

/**
 * This is the model class for table "items".
 *
 * The followings are the available columns in table 'items':
 * @property integer $id
 * @property integer $parent_id
 * @property integer $category_id
 * @property string $art
 * @property string $name
 * @property string $full_desc
 * @property string $short_desc
 * @property string $photo
 * @property string $portion
 * @property double $price
 * @property string $info
 */
class Items extends CActiveRecord
{ public $cat_cat_id;
public $cat_cat_cat_id;


	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Items the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
		$this->menu=Yii::app()->session['menu'];
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'items';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('category_id, art, name,', 'required'),
			array('parent_id, category_id', 'numerical', 'integerOnly'=>true),
			array('price', 'numerical'),
			array('art', 'length', 'max'=>11),
			array('photo, portion', 'length', 'max'=>155),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, parent_id, category_id, art, name, full_desc, short_desc, photo, portion, price, info', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
		'parent_item' => array(self::BELONGS_TO, 'Items', 'parent_id'),
		'children_items' => array(self::HAS_MANY, 'Items', 'parent_id'),
		'checkminiitems_open' => array(self::STAT, 'CheckMiniItem', 'item_id','condition'=>'parent_id=:checkmini_id',
		'params'=>array('checkmini_id'=>$this->getCheckId(1)), 'select' => 'SUM(count)'),
		'checkminiitems_send' => array(self::STAT, 'CheckMiniItem', 'item_id','condition'=>'parent_id IN ('.$this->getCheckId(2).')', 'select' => 'SUM(count)','order'=>"id DESC"),
		'items_xref_children' => array(self::MANY_MANY, 'ItemXref', '(child_id, parent_id)'),
		'items_xref_parent' => array(self::MANY_MANY, 'ItemXref', '(parent_id,child_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'parent_id' => 'Родительский товар',
			'category_id' => 'Категория',
			'art' => 'Артикул',
			'name' => 'Наименование',
			'full_desc' => 'Полное описание',
			'short_desc' => 'Краткое описание',
			'photo' => 'Фото',
			'portion' => 'Порция',
			'price' => 'Цена',
			'info' => 'Информация',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('art',$this->art,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('full_desc',$this->full_desc,true);
		$criteria->compare('short_desc',$this->short_desc,true);
		$criteria->compare('photo',$this->photo,true);
		$criteria->compare('portion',$this->portion,true);
		$criteria->compare('price',$this->price);
		$criteria->compare('info',$this->info,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function getCheckId($status)
	{ //определяем сколько заказов есть у этого товара
		$checkmini_id='';
		$menu=Yii::app()->session['menu'];
		if(isset($menu['check_id'])) {
			if($status==2)
			{ $checksminiIdSend='';
				$checkminisend=CheckMini::model()->findAllByAttributes(
				array('check_id'=>$menu['check_id'],'status'=>2),
				array('order'=>'id DESC'));
					foreach($checkminisend as $check)
					{$checksminiIdSend[]=$check->id;}
					
					if(!empty($checksminiIdSend)) 
			$checkmini_id=implode(',',$checksminiIdSend);
			if (!$checkmini_id) $checkmini_id=0;
			
			}
			else
			{ 
			if($modelCheckMini=CheckMini::model()->findByAttributes(
			array('check_id'=>$menu['check_id'],'status'=>$status),
			array('order'=>'id DESC')))
				{$checkmini_id=$modelCheckMini->id; }
				
			}
		}	
		else {if($status==2) $checkmini_id=0;}
		if(!isset($checkmini_id)) $checkmini_id='';
		
		return $checkmini_id;
		
	}
}