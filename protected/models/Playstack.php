<?php

/**
 * This is the model class for table "playstack".
 *
 * The followings are the available columns in table 'playstack':
 * @property integer $id
 * @property integer $track_id
 * @property integer $table_id
 * @property integer $cdate
 * @property integer $position
 */
class Playstack extends CActiveRecord
{
	public $countid;
	public $maxposition;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Playstack the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'playstack';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, track_id, table_id, cdate, position', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'track_id' => 'Track',
			'table_id' => 'Table',
			'cdate' => 'Cdate',
			'position' => 'Position',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('track_id',$this->track_id);
		$criteria->compare('table_id',$this->table_id);
		$criteria->compare('cdate',$this->cdate);
		$criteria->compare('position',$this->position);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}