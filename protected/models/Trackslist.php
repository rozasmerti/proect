<?php

/**
 * This is the model class for table "trackslist".
 *
 * The followings are the available columns in table 'trackslist':
 * @property integer $id
 * @property integer $track_id
 * @property integer $check_id
 * @property integer $table_id
 * @property integer $cdate
 * @property integer $tracksize
 * @property integer $trackactive
 * @property integer $position
 */
class Trackslist extends CActiveRecord
{
	public $countid;
	public $trackname;
	public $avtor;
	public $t2;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Trackslist the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'trackslist';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, track_id, check_id, table_id, cdate, tracksize, trackactive, position', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'track_id' => 'Track',
			'check_id' => 'Check',
			'table_id' => 'Table',
			'cdate' => 'Cdate',
			'tracksize' => 'Tracksize',
			'trackactive' => 'Trackactive',
			'position' => 'Position',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('track_id',$this->track_id);
		$criteria->compare('check_id',$this->check_id);
		$criteria->compare('table_id',$this->table_id);
		$criteria->compare('cdate',$this->cdate);
		$criteria->compare('tracksize',$this->tracksize);
		$criteria->compare('trackactive',$this->trackactive);
		$criteria->compare('position',$this->position);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}