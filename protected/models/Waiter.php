<?php

/**
 * This is the model class for table "waiter".
 *
 * The followings are the available columns in table 'waiter':
 * @property integer $id
 * @property string $name
 * @property string $password
 * @property string $salt
 * @property integer $status
 */
class Waiter extends CActiveRecord
{
	public $username;
	public $password;
	public $rememberMe;

	private $_identity;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Waiter the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'waiter';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, name, password', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('password, salt', 'length', 'max'=>155),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, password, salt, status', 'safe', 'on'=>'search'),
			
			array('rememberMe', 'boolean'),
			// password needs to be authenticated
			array('password', 'authenticate'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'status_name' => array(self::BELONGS_TO, 'WaiterStatus', 'status'),
		'checks' => array(self::STAT, 'Check', 'user_id','condition'=>'status=1'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Имя',
			'password' => 'Пароль',
			'salt' => 'Salt',
			'status' => 'Статус',
			'rememberMe'=>'Запомнить меня',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('salt',$this->salt,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute,$params)
	{
		if(!$this->hasErrors())
		{
			$this->_identity=new UserIdentity($this->id,$this->password);
			if(!$this->_identity->authenticate())
			//$this->addError('password',$this->_identity->errorCode);
				$this->addError('password','Неправильный пароль');
				
		}
	}
public function validatePassword($password)
    {
        return $this->hashPassword($password,$this->salt)===$this->password;
    }
 
    public function hashPassword($password,$salt)
    {
        return md5($salt.$password);
    }

	/**
	 * Logs in the user using the given username and password in the model.
	 * @return boolean whether login is successful
	 */
	public function login()
	{  
		if($this->_identity===null)
		{ 
			$this->_identity=new UserIdentity($this->id,$this->password);
			$this->_identity->authenticate();
		}
		if($this->_identity->errorCode===UserIdentity::ERROR_NONE)
		{
			$duration=$this->rememberMe ? 3600*24*30 : 0; // 30 days
			Yii::app()->user->login($this->_identity,$duration);
			return true;
		}
		else
			return false;
	}
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}
	