<?php
/* @var $this CheckMiniItemController */
/* @var $model CheckMiniItem */

$this->breadcrumbs=array(
	'Check Mini Items'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CheckMiniItem', 'url'=>array('index')),
	array('label'=>'Manage CheckMiniItem', 'url'=>array('admin')),
);
?>

<h1>Create CheckMiniItem</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>