<?php
/* @var $this CheckMiniItemController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Check Mini Items',
);

$this->menu=array(
	array('label'=>'Create CheckMiniItem', 'url'=>array('create')),
	array('label'=>'Manage CheckMiniItem', 'url'=>array('admin')),
);
?>

<h1>Check Mini Items</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
