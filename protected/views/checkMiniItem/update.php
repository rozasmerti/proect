<?php
/* @var $this CheckMiniItemController */
/* @var $model CheckMiniItem */

$this->breadcrumbs=array(
	'Check Mini Items'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List CheckMiniItem', 'url'=>array('index')),
	array('label'=>'Create CheckMiniItem', 'url'=>array('create')),
	array('label'=>'View CheckMiniItem', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage CheckMiniItem', 'url'=>array('admin')),
);
?>

<h1>Update CheckMiniItem <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>