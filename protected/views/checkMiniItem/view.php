<?php
/* @var $this CheckMiniItemController */
/* @var $model CheckMiniItem */

$this->breadcrumbs=array(
	'Check Mini Items'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List CheckMiniItem', 'url'=>array('index')),
	array('label'=>'Create CheckMiniItem', 'url'=>array('create')),
	array('label'=>'Update CheckMiniItem', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete CheckMiniItem', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CheckMiniItem', 'url'=>array('admin')),
);
?>

<h1>View CheckMiniItem #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'parent_id',
		'item_id',
		'count',
		'summ',
		'info',
	),
)); ?>
