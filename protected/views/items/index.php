

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'/items/_view',
	'itemsCssClass'=>'mode mode_list_one',
	'enablePagination'=>false,
	'summaryText'=>'',
)); ?>
