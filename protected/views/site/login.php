<div class="auth_form">
<div class="close"></div>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	 'enableAjaxValidation' => true,
    'clientOptions' => array(
      'validateOnSubmit' => true,
      'validateOnChange' => false,
	  'afterValidate' => 'js:function(form, data, hasError){alert("hasError"); return false;}',
    ),

)); 

?>

	

	<div class="row">
		<?php echo $form->hiddenField($model,'name'); ?>
		<?php echo $form->error($model,'name'); ?>
		<?php echo $form->hiddenField($model,'id'); ?>
		<?php echo $form->error($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->passwordField($model,'password'); ?>
		<a href="javascript:delSimvol()" class="back"></a>
		<?php echo $form->error($model,'password'); ?>
	</div>

	<div class="num_buttons">
	 <div class="str_num">
	 <span>7</span>
	 <span>8</span>
	 <span>9</span>
	 </div>
	 <div class="str_num">
	 <span>4</span>
	 <span>5</span>
	 <span>6</span>
	 </div>
	 <div class="str_num">
	 <span>1</span>
	 <span>2</span>
	 <span>3</span>
	 </div>
	</div>

	<div class="row buttons">
	<div class="info">
	<?php echo $name_waiter ?>
	<p>Введите пароль</p>
		</div>
		<?php echo CHtml::ajaxSubmitButton('Вход','index.php?r=waiter/login',array(
      'type' => 'JSON',
      'success'=>'function(html)
	  { 
		
	  $(\'body\').html(html);
	  }',
    ), array('type' => 'submit','id'=>'loginsubmit'.uniqid())); ?>
	<div class="str_num">
	 <span>0</span>
	 </div>
	</div>

<?php $this->endWidget(); ?>
</div>

</div><!-- form -->